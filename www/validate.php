<?php

//-----------------------------------------------
//Check array for empty values
//Return true if found
function findEmpty($array) {
	
	$empty = false;
	$length = sizeof($array);
	
	//Loop through every element in $array
	for ($i = 0; $i < $length; $i++){
		
		//Check for empty value
		if(empty($array[$i])){
		
			//Break the loop
			$i = $length;
			
			//Set return value
			$empty = true;
		}
	}
	return $empty;
}

//-----------------------------------------------
//Check array for non-numeric values
//Return true if found
function findNonNumeric($array){
	
	$non_numeric = false;
	$length = sizeof($array);
	
	//Loop through every element in $array
	for ($i = 0; $i < $length; $i++){
		
		//Check for numeric value
		if(!is_numeric($array[$i])){
		
			//Break the loop
			$i = $length;
			
			//Set return value
			$non_numeric = true;
		}
	}
	return $non_numeric;
}

//-----------------------------------------------
//Check array for null values
//Return true if found
function findNotSet($array){

	$not_set = false;
	$length = sizeof($array);
	
	//Loop through every element in $array
	for ($i = 0; $i < $length; $i++){
		
		//Check for null value
		if(!isset($array[$i])){
		
			//Break the loop
			$i = $length;
			
			//Set return value
			$not_set = true;
		}
	}
	return $not_set;
}


?>