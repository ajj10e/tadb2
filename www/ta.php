<?php
	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
?>
<html>
	<head>
		<title>TADB - TA</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
		<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css"/> <link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDel(id)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+"?")
				if(opt==true)
					window.location.href = "delete.php?type=ta&id="+id;
			}
		</script>
		<div class="container">
			<h1>TA</h1>
			<div class="table-responsive">
			<table id="myTable" class="table" >
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<?php
				try{
					$statement = $db->prepare("SELECT ta_id,ta_fname,ta_lname,ta_email,ta_teaching_interests,ta_research_interests FROM ta ORDER BY ta_lname,ta_fname");
				/*	$statement->bindParams(":taId", "ta_id");
					$statement->bindParams(":taFname", "ta_fname");
					$statement->bindParams(":taLname", "ta_lname");
					$statement->bindParams(":taEmail", "ta_email");
					$statement->bindParams(":taTeachingInterests", "ta_teaching_interests");
					$statement->bindParams(":taResearchIntrests", "ta_research_interests");
					$statement->bindParams(":ta", "ta");
				*/	$statement->execute();
					$statement->setFetchMode(PDO::FETCH_ASSOC);
					//$res = $mysqlconn->query("SELECT ta_id,ta_fname,ta_lname,ta_email,ta_teaching_interests,ta_research_interests FROM ta ORDER BY ta_lname,ta_fname");
				}catch (PDOException $e){
					$e->getMessage();
					sleep(10);
					die();
				}
					while($row = $statement->fetch())
					{
						echo "<tr>";
						echo "<td>";
							echo htmlspecialchars($row['ta_lname']) . ', ' . htmlspecialchars($row['ta_fname']);
						echo "</td>";
						echo "<td>";
							echo htmlspecialchars($row['ta_email']);
						echo "</td>";
						echo "<td>";
							echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_ta.php?id=".$row['ta_id']."\">Edit</a>";
						echo "</td>";
						echo "<td>";
							echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDel(".$row['ta_id'].")\">Delete</button>";
						echo "</td>";
						echo "</tr>";
					}
				?>
			</table>
			</div>
			<hr />
			<h2>Add TA</h2>
			<form name="add_ta" action="add.php" method="post">
				<input type="hidden" name="type" value="ta" />
				First name: <input type="text" class="form-control" name="t_fname" />
				Last name: <input type="text" class="form-control" name="t_lname" />
				E-mail: <input type="email" class="form-control" name="t_email" />
				Teaching interests: <input type="text" class="form-control" name="t_t_i" />
				Research interests: <input type="text" class="form-control" name="t_r_i" />
				<input type="submit" class="btn btn-primary btn-lg" value="Add" />
			</form>
		</div>
		<?php 
		include 'includes/footer.php';
		$db = null;
		?>
	</body>
	<script>
		$(document).ready(function(){
			$('#myTable').dataTable({
				"columns": [
					null,
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
		});
	</script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>
</html>