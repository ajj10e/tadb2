<?php
	$cfg = include_once('includes/config.php');
	include_once('includes/connection.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['ta'])
		die('Access denied.');
	$taid;
	
	try {
		$stat0 = $db->prepare("SELECT usr_id FROM user WHERE usr_email=:email");
		$stat0->bindParam(":email", $_SESSION['email']);
		$stat0->execute();
		$stat0->setFetchMode(PDO::FETCH_ASSOC);
	}
	catch(PDOException $e) {
		include_once('includes/error.php');
	}
	
	$numberOfRows = 0;
	$testingMode = true;
	if ($stat0->rowCount() != 1 && $testingMode === false){
		die("TA with e-mail ".$_SESSION['email']." is not in the list of TAs. Contact administrator.");
	}
	while ($row = $stat0->fetch()){
		$taid = $row['usr_id'];
	}
	if ( ! isset($taid) || $taid == null){
		$taid = 1;
	}
	//$res = $mysqlconn->query("SELECT ta_id FROM ta WHERE ta_email='".$_SESSION['email']."'");
	//if($res->num_rows != 1)
	//	die("TA with e-mail ".$_SESSION['email']." is not in the list of TAs. Contact administrator.");
	//while($row = $res->fetch_assoc())
	//	$taid = $row['ta_id'];
?>
<html>
	<head>
		<title>TADB - TA Profile</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDelA(id)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+"?")
				if(opt==true)
					window.location.href = "delete.php?type=tapa&id="+<?php echo $taid ?>+"&id2="+id;
			}
			function confirmDelS(id)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+"?")
				if(opt==true)
					window.location.href = "delete.php?type=taps&id="+<?php echo $taid ?>+"&id2="+id;
			}
		</script>
		<div class="container">
			<h1>TA Profile</h1>
			<h2>Interests</h2>
			<?php 
			try{
				$stat1 = $db->prepare("SELECT * FROM ta WHERE ta_id=:taid;");
				$stat1->bindParam(":taid", $taid);
				$ri = '';
				$ti = '';
				$stat1->execute();
				$stat1->setFetchMode(PDO::FETCH_ASSOC);
			}
			catch(PDOException $e) {
				include_once('includes/error.php');
			}
			
			
			//$res = $mysqlconn->query("SELECT ta_teaching_interests,ta_research_interests FROM ta WHERE ta_id = $taid");
			/*
				$ti = $ri = '';
				while($row = $res->fetch_assoc())
				{
					$ti = $row['ta_teaching_interests'];
					$ri = $row['ta_research_interests'];
				}
			*/
			?>
			<form name="tap_interests" action="edit_ta_profile.php" method="post">
				Teaching Interests: <input type="text" class="form-control" name="tap_ti" value="<?php echo $ti; ?>" />
				Research Interests: <input type="text" class="form-control" name="tap_ri" value="<?php echo $ri; ?>" />
				<input type="submit" class="btn btn-primary btn-lg" value="Update" />
			</form>
				<h2>Skills</h2>
				<table id="skills" class="table table-striped table-condensed" >
					<thead>
						<tr>
							<th>Skill</th>
							<th>Rating</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<?php
						if ($testingMode === true){
							$taid = 1;
						}
						
						try {
							$stat99 = $db->prepare("SELECT skl_id, skl_name, skl_rating FROM skill WHERE ta_id=$taid;");
							$stat99->execute();
							$stat99->setFetchMode(PDO::FETCH_ASSOC);
						}
						catch(PDOException $e) {
							include_once('includes/error.php');
						}
						
						while ($row = $stat99->fetch()){
							echo "<tr>";
							echo "<td>";
							echo htmlspecialchars($row['skl_name']);
							echo '</td>';
							echo '<td>';
							echo htmlspecialchars($row['skl_rating']);
							echo '</td>';
							echo '<td>';
							echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_ta_profile.php?sklid=".$row['skl_id']."\">Edit</a>";
							echo "</td>";
							echo "<td>";
								echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDelS(".$row['skl_id'].")\">Delete</button>";
							echo "</td>";
							echo '</tr>';
						}
					/*
						$res = $mysqlconn->query("SELECT ta_skill.skl_id,tskl_rating,skl_name FROM ta_skill NATURAL JOIN skill WHERE ta_id = $taid ORDER BY tskl_rating");
						while($row = $res->fetch_assoc())
						{
							echo "<tr>";
							echo "<td>";
								echo $row['skl_name'];
							echo "</td>";
							echo "<td>";
								echo $row['tskl_rating'];
							echo "</td>";
							echo "<td>";
								echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_ta_profile.php?sklid=".$row['skl_id']."\">Edit</a>";
							echo "</td>";
							echo "<td>";
								echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDelS(".$row['skl_id'].")\">Delete</button>";
							echo "</td>";
							echo "</tr>";
						}
					*/
					?>
				<table>
				<h3>Add Skill</h3>
				<form name="tap_skills" action="add.php" method="post">
					<input type="hidden" name="type" value="tapskill" />
					<input type="hidden" name="tap_tid" value="<?php echo $taid; ?>">
					Skill: <select class="form-control" name="tap_skl">
									<?php
										try {
											$stat2 = $db->prepare("SELECT skl_id, skl_name FROM skill;");
											$stat2->execute();
											$stat2->setFetchMode(PDO::FETCH_ASSOC);
											while ($row = $stat2->fetch()){
												echo "<option value=\"".$row['skl_id']."\">".$row['skl_name']."</option>";
											}
										}
										catch(PDOException $e) {
											include_once('includes/error.php');
										}
										
										
									?>
								</select>
					Rating: <input type="number" class="form-control" name="tap_rating" min="1" max="10" />
					<input type="submit" class="btn btn-primary btn-lg" value="Add skill" />
				</form>
				<h2>Areas</h2>
				<table id="areas" class="table table-striped table-condensed" >
					<thead>
						<tr>
							<th>Area Name</th>
							<th>FTE</th>
							<th>Semester</th>
							<th>Year</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<?php
						try {
							$stat3 = $db->prepare("SELECT area_id, area_name, area_fte, area_semester, area_year FROM ta_area NATURAL JOIN area WHERE ta_id = $taid ORDER BY area_name;");
							$stat3->execute();
							$stat3->setFetchMode(PDO::FETCH_ASSOC);
							while ($row = $stat3->fetch()){
								echo "<tr>";
								echo "<td>";
									echo $row['area_name'];
								echo "</td>";
								echo "<td>";
									echo $row['area_fte'];
								echo "</td>";
								echo "<td>";
									echo $row['area_semester'];
								echo "</td>";
								echo "<td>";
									echo $row['area_year'];
								echo "</td>";
								echo "<td>";
									echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_ta_profile.php?&aid=".$row['area_id']."\">Edit</a>";
								echo "</td>";
								echo "<td>";
									echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDelA(".$row['area_id'].")\">Delete</button>";
								echo "</td>";
								echo "</tr>";
							}
						}
						catch(PDOException $e) {
							include_once('includes/error.php');
						}
						
					/*
						$res = $mysqlconn->query("SELECT area_id,area_name,area_fte,area_semester,area_year FROM ta_area NATURAL JOIN area WHERE ta_id = $taid ORDER BY area_name");
						while($row = $res->fetch_assoc())
						{
							echo "<tr>";
							echo "<td>";
								echo $row['area_name'];
							echo "</td>";
							echo "<td>";
								echo $row['area_fte'];
							echo "</td>";
							echo "<td>";
								echo $row['area_semester'];
							echo "</td>";
							echo "<td>";
								echo $row['area_year'];
							echo "</td>";
							echo "<td>";
								echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_ta_profile.php?&aid=".$row['area_id']."\">Edit</a>";
							echo "</td>";
							echo "<td>";
								echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDelA(".$row['area_id'].")\">Delete</button>";
							echo "</td>";
							echo "</tr>";
						}
						*/
					?>
				</table>
				<h3>Add Area</h3>
				<form name="tap_areas" action="add.php" method="post">
					<input type="hidden" name="type" value="taparea" />
					<input type="hidden" name="tap_tid" value="<?php echo $taid; ?>">
					Area: <select class="form-control" name="tap_aid">
							<?php
								try {
									$stat4 = $db->prepare("SELECT area_id, area_name FROM area ORDER by area_name;");
									$stat4->execute();
									$stat4->setFetchMode(PDO::FETCH_ASSOC);
									while ($row = $stat4->fetch()){
										echo "<option value=\"".$row['area_id']."\">".$row['area_name']."</option>";
									}
								}
								catch(PDOException $e) {
									include_once('includes/error.php');
								}
							/*
								$res3 = $mysqlconn->query("SELECT area_id,area_name FROM area ORDER BY area_name");
								while ($row = $res3->fetch_assoc())
									echo "<option value=\"".$row['area_id']."\">".$row['area_name']."</option>";
							*/
							?>
						</select>
					FTE: <input type="number" class="form-control" name="tap_fte" min="0" max="9.99" step="0.01" value="1.00" />
					Semester: <select class="form-control" name="tap_sem">
						<option value="fall">Fall</option>
						<option value="spr">Spring</option>
						<option value="sum">Summer</option>
					</select>
					Year: <input type="number" class="form-control" name="tap_year" min="<?php echo intval(date("Y"))-10; ?>" max="<?php echo intval(date("Y"))+10; ?>" value="<?php echo intval(date("Y")); ?>" />
					<input type="submit" class="btn btn-primary btn-lg" value="Add area" />
				</form>
		</div>
		<?php include 'includes/footer.php'; ?>
		<?php
			$db = null;
		?>
	</body>
	<script>
		$(document).ready(function(){
			$('#areas').dataTable({
				"columns": [
					null,
					null,
					null,
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
			$('#skills').dataTable({
				"columns": [
					null,
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
		});
	</script>
</html>