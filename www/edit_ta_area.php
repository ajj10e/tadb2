<?php
	
	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	
	$taid = $aid = $fte = $sem = $year = $taname = $aname = 0;

	if(isset($_POST['taa_tid']) && isset($_POST['taa_aid']))
	{
		
		try {
			$stmt = $db->prepare('UPDATE ta_area 
								  SET area_id=:taa_areaid,ta_id=:taa_taid,area_fte=:taa_fte,area_semester=:taa_sem,area_year=:taa_year 
								  WHERE area_id=:taa_aid AND ta_id=:taa_tid');
			$stmt->bindParam(':taa_areaid', $_POST['taa_areaid']);
			$stmt->bindParam(':taa_taid', 	$_POST['taa_taid']);
			$stmt->bindParam(':taa_fte', 	$_POST['taa_fte']);
			$stmt->bindParam(':taa_sem', 	$_POST['taa_sem']);
			$stmt->bindParam(':taa_year', 	$_POST['taa_year']);
			$stmt->bindParam(':taa_aid', 	$_POST['taa_aid']);
			$stmt->bindParam(':taa_tid', 	$_POST['taa_tid']);
			$stmt->bindParam(':taa_sem', 	$_POST['taa_sem']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		header("Location: ta_area.php");
		die('Redirecting...');
	}elseif(isset($_GET['taid']) && isset($_GET['aid'])){
		$taid = $_GET['taid'];
		$aid = $_GET['aid'];
		
		try {
			$stmt = $db->prepare('SELECT area_fte,area_semester,area_year 
								 FROM ta_area 
								 NATURAL JOIN ta 
								 NATURAL JOIN area 
								 WHERE ta_id=:taid AND area_id=:aid 
								 ORDER BY ta_lname,ta_fname,area_name');
			$stmt->bindParam(':taid', $taid);
			$stmt->bindParam(':aid', $aid);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$res = $stmt->fetch();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		$fte = $res['area_fte'];
		$sem = $res['area_semester'];
		$year = $res['area_year'];
	}
	else
		die("No ID provided");
?>
<html>
	<head>
		<title>TADB - TA Area Edit</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
	<body>
		<?php include 'includes/header.php'; ?>
		<div class="container">
			<h2>Edit TA Area</h2>
			<form name="edit_ta_area" action="edit_ta_area.php" method="post">
				<input type="hidden" name="taa_tid" value="<?php echo $taid; ?>" />
				<input type="hidden" name="taa_aid" value="<?php echo $aid; ?>" />
				TA: <select class="form-control" name="taa_taid">
						<?php
							$res2 = $db->query("SELECT ta_id,CONCAT(ta_lname,', ',ta_fname) as ta_name FROM ta ORDER BY ta_lname,ta_fname");
							$res2->setFetchMode(PDO::FETCH_ASSOC);
							while($row = $res2->fetch())
							{
								$selopt = '';
								if($row['ta_id'] == $taid)
									$selopt = ' selected';
								echo "<option value=\"".$row['ta_id']."\"$selopt>".$row['ta_name']."</option>";
							}
						?>
					</select>
				Area: <select class="form-control" name="taa_areaid">
						<?php
							$res3 = $db->query("SELECT area_id,area_name FROM area ORDER BY area_name");
							$res3->setFetchMode(PDO::FETCH_ASSOC);
							while($row = $res3->fetch())
							{
								$selopt = '';
								if($row['area_id'] == $aid)
									$selopt = ' selected';
								echo "<option value=\"".$row['area_id']."\"$selopt>".$row['area_name']."</option>";
							}
						?>
					</select>
				Area FTE: <input type="number" class="form-control" name="taa_fte" min="0" max="9.99" step="0.01" value="<?php echo $fte ?>" />
				Area Semester: <select class="form-control" name="taa_sem">
					<option value="fall">Fall</option>
					<option value="spr">Spring</option>
					<option value="sum">Summer</option>
				</select>
				Area Year: 
				<select class="form-control" name="a_year">
					<option value="2000">2000</option>
					<option value="2001">2001</option>
					<option value="2002">2002</option>
					<option value="2003">2003</option>
					<option value="2004">2004</option>
					<option value="2005">2005</option>
					<option value="2006">2006</option>
					<option value="2007">2007</option>
					<option value="2008">2008</option>
					<option value="2009">2009</option>
					<option value="2010">2010</option>
					<option value="2011">2011</option>
					<option value="2012">2012</option>
					<option value="2013">2013</option>
					<option value="2014">2014</option>
					<option value="2015">2015</option>
					<option value="2016">2016</option>
					<option value="2017">2017</option>
					<option value="2018">2018</option>
					<option value="2019">2019</option>
					<option value="2020">2020</option>
				</select>
				<input type="submit" class="btn btn-primary btn-lg" value="Edit" />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
		<?php $db = null; ?>
	</body>
</html>