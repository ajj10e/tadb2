<?php


	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	
	$aid = NULL;
	$aname = '';
	
	//If a_id is not empty, update the record.
	//If a_id is empty, but id is not, retrieve all area names and store in res.
	//Otherwise, die.
	if(!empty($_POST['a_id']))
	{
		try{
			$stmt = $db->prepare('UPDATE area 
								  SET area_name=:a_name 
								  WHERE area_id=:a_id');
			$stmt->bindParam(':a_name', $_POST['a_name']);
			$stmt->bindParam(':a_id', $_POST['a_id']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		header("Location: area.php");
		die('Redirecting...');
	}elseif(!empty($_GET['id'])){
	
		$aid = $_GET['id'];
		
		try{
			$stmt = $db->prepare('SELECT area_name 
								  FROM area 
								  WHERE area_id=:a_id');
			$stmt->bindParam(':a_id', $aid);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$res = $stmt->fetch();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		$aname = isset($res['area_name']) ? $res['area_name'] : '';
	}
	else
		die("No ID provided");
?>
<html>
	<head>
		<title>TADB - Area Edit</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
	<body>
		<?php include 'includes/header.php'; ?>
		<div class="container">
			<h2>Edit Area</h2>
			<form name="edit_area" action="edit_area.php" method="post">
				<input type="hidden" name="a_id" value="<?php echo $aid; ?>" />
				Area Name: <input type="text" class="form-control" name="a_name" value="<?php echo $aname; ?>" />
				<input type="submit" class="btn btn-primary btn-lg" value="Edit" />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
		<?php $db = null; ?>
	</body>
</html>