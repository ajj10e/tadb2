<?php
	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['ta'])
		die('Access denied.');
	
	$table = $_POST['type'];

	$query = NULL;
	$querystring = "";

	if($table == "course") {
		
		/*
		if(strlen(trim($_POST['c_name'])) < 1)
			die('Need a valid course name.');
			*/
			
		$querystring = "INSERT INTO course (crs_num,crs_name,crs_description) VALUES (:c_num,:c_name,:c_desc)";
		try {
			$stmt = $db->prepare($querystring);
			$stmt->bindParam(':c_num', $_POST['c_num']);
			$stmt->bindParam(':c_name', $_POST['c_name']);
			$stmt->bindParam(':c_desc', $_POST['c_desc']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "area") {
	
		$querystring = "INSERT INTO area (area_name) VALUES (:a_name)";
		try {
			$stmt = $db->prepare($querystring);
			$stmt->bindParam(':a_name', $_POST['a_name']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "faculty") {
	
		if(!isset($_POST['f_email']) || !(preg_match("/([A-Za-z0-9]+)@([A-Za-z0-9]*)(\.?)fsu\.edu/",$_POST['f_email'])))
			die("Invalid E-mail");
			
		$querystring = "INSERT INTO faculty (fac_fname,fac_lname,fac_email) VALUES (:f_fname,:f_lname,:f_email)";
		try {
			$stmt = $db->prepare($querystring);
			$stmt->bindParam(':f_fname', $_POST['f_fname']);
			$stmt->bindParam(':f_lname', $_POST['f_lname']);
			$stmt->bindParam(':f_email', $_POST['f_email']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "ta") {
	
		if(!isset($_POST['t_email']) || !(preg_match("/([A-Za-z0-9]+)@([A-Za-z0-9]*)(\.?)fsu\.edu/",$_POST['t_email'])))
			die("Invalid E-mail");
			
		$querystring = "INSERT INTO ta (ta_fname,ta_lname,ta_email,ta_teaching_interests,ta_research_interests) VALUES (:t_fname,:t_lname,:t_email,:t_t_i,:t_r_i)";
		try {
			$stmt = $db->prepare($querystring);
			$stmt->bindParam(':t_fname', $_POST['t_fname']);
			$stmt->bindParam(':t_lname', $_POST['t_lname']);
			$stmt->bindParam(':t_email', $_POST['t_email']);
			$stmt->bindParam(':t_t_i', $_POST['t_t_i']);
			$stmt->bindParam(':t_r_i', $_POST['t_r_i']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "skill") {
	
		$querystring = "INSERT INTO skill (skl_name,skl_description) VALUES (:s_name,:s_desc)";
		try {
			$stmt = $db->prepare($querystring);
			$stmt->bindParam(':s_name', $_POST['s_name']);
			$stmt->bindParam(':s_desc', $_POST['s_desc']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "ta_skill") {
	
		$querystring = "INSERT INTO ta_skill (ta_id,skl_id,tskl_rating) VALUES(:tas_ta,:tas_skl,:tas_rating)";
		try {
			$stmt = $db->prepare($querystring);
			$stmt->bindParam(':tas_ta', $_POST['tas_ta']);
			$stmt->bindParam(':tas_skl', $_POST['tas_skl']);
			$stmt->bindParam(':tas_rating', $_POST['tas_rating']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "ta_area") {
		
		$querystring = "INSERT INTO ta_area (area_id,ta_id,area_fte,area_semester,area_year) VALUES(:taa_aid,:taa_ta,:a_fte,:a_sem,:a_year)";
		try {
			$stmt = $db->prepare($querystring);
			$stmt->bindParam(':taa_aid', $_POST['taa_aid']);
			$stmt->bindParam(':taa_ta', $_POST['taa_ta']);
			$stmt->bindParam(':a_fte', $_POST['a_fte']);
			$stmt->bindParam(':a_sem', $_POST['a_sem']);
			$stmt->bindParam(':a_year', $_POST['a_year']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "course_skill") {
		
		$querystring = "INSERT INTO course_skill (crs_id,fac_id,skl_id) VALUES(:c_id,:f_id,:s_id)";
		try {
			$stmt = $db->prepare($querystring);
			$stmt->bindParam(':f_id', $_POST['f_id']);
			$stmt->bindParam(':s_id', $_POST['s_id']);
			$stmt->bindParam(':c_id', $_POST['c_id']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "assignment") {
		
		$querystring = "INSERT INTO assignment (ta_id,fac_id,crs_id,asn_semester,asn_year,asn_reason) VALUES(:a_tid,:a_fid,:a_cid,:a_sem,:a_year,:a_reason)";
		try {
			$stmt = $db->prepare($querystring);
			$stmt->bindParam(':a_tid', $_POST['a_tid']);
			$stmt->bindParam(':a_fid', $_POST['a_fid']);
			$stmt->bindParam(':a_cid', $_POST['a_cid']);
			$stmt->bindParam(':a_sem', $_POST['a_sem']);
			$stmt->bindParam(':a_year', $_POST['a_year']);
			$stmt->bindParam(':a_reason', $_POST['a_reason']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "tapskill")
	{
		$table = "ta_profile";
		$querystring = "INSERT INTO ta_skill (ta_id,skl_id,tskl_rating) VALUES(:tap_tid,:tap_skl,:tap_rating)";
		try {
			$stmt = $db->prepare($querystring);
			$stmt->bindParam(':tap_tid', $_POST['tap_tid']);
			$stmt->bindParam(':tap_skl', $_POST['tap_skl']);
			$stmt->bindParam(':tap_rating', $_POST['tap_rating']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "taparea")
	{
		$table = "ta_profile";
		$querystring = "INSERT INTO ta_area (ta_id,area_id,area_fte,area_semester,area_year) VALUES(:tap_tid,:tap_aid,:tap_fte,;tap_sem,:tap_year)";
		try {
			$stmt = $db->prepare($querystring);
			$stmt->bindParam(':tap_tid', $_POST['tap_tid']);
			$stmt->bindParam(':tap_aid', $_POST['tap_aid']);
			$stmt->bindParam(':tap_fte', $_POST['tap_fte']);
			$stmt->bindParam(':tap_sem', $_POST['tap_sem']);
			$stmt->bindParam(':tap_year', $_POST['tap_year']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	else
		die("Invalid form type!");
		
	header('Refresh: 0;URL='.$table.'.php');
	echo "Added new record";
	echo "<br/>Please wait while you are redirected...";

	//$query = $mysqlconn->query($querystring);
	
	/*
	try {
		$stmt = $db->prepare($querystring);
		$stmt->bindParam();
	}
	catch(PDOException $e) {
		include_once('includes/error.php');
	}

	if($query)
	{
		header('Refresh: 0;URL='.$table.'.php');
		echo "Added new record";
		echo "<br/>Please wait while you are redirected...";
	}
	else
		echo "Failed to add new record. ".$mysqlconn->error;
	*/
?> ". ."