<?php
//TODO
	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['ta'])
		die('Access denied.');
	$taid;
	
	try {
		$stmt = $db->prepare('SELECT ta_id
							  FROM ta
							  WHERE ta_email=:email');
		$stmt->bindParam(':email', $_SESSION['email']);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		
		$res = $stmt->fetch();
		$taid = $res['ta_id'];
	}
	catch(PDOException $e) {
		include_once('includes/error.php');
	}
	
	$sklid = $aid = 0;
	$sklrating = 0;
	$afte = $asem = $ayear = 0;
	$disp = -1;
	
	if(isset($_POST['tap_ti']) || isset($_POST['tap_ri']))
	{
		//$mysqlconn->query("UPDATE ta SET ta_teaching_interests='".$_POST['tap_ti']."' WHERE ta_id=$taid");
		
		try {
			$stmt = $db->prepare('UPDATE ta
								  SET ta_teaching_interests=:tap_ti, ta_research_interests=:tap_ri
								  WHERE ta_id=:taid');
			$stmt->bindParam(':tap_ti', $_POST['tap_ti']);
			$stmt->bindParam(':tap_ri', $_POST['tap_ri']);
			$stmt->bindParam(':taid', $taid);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		header("Location: ta_profile.php");
		die("Redirecting...");
	}
	
	if(isset($_POST['tap_s']) && isset($_POST['tap_skl']) && isset($_POST['tap_rating']))
	{
		//$mysqlconn->query("UPDATE ta_skill SET skl_id=".$_POST['tap_skl'].",tskl_rating=".$_POST['tap_rating']." WHERE ta_id=$taid AND skl_id=".$_POST['tap_s']);
		
		try {
			$stmt = $db->prepare('UPDATE ta_skill
								  SET skl_id=:tap_skl, tskl_rating=:tap_rating
								  WHERE ta_id=:taid AND skl_id=:tap_s');
			$stmt->bindParam(':tap_skl', 	$_POST['tap_skl']);
			$stmt->bindParam(':tap_rating', $_POST['tap_rating']);
			$stmt->bindParam(':taid', 		$taid);
			$stmt->bindParam(':tap_s', 		$_POST['tap_s']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		header("Location: ta_profile.php");
		die("Redirecting...");
	}
	
	if(isset($_POST['tap_a']) && isset($_POST['tap_aid']) && isset($_POST['tap_fte']) && isset($_POST['tap_sem']) && isset($_POST['tap_year']))
	{
		$mysqlconn->query("UPDATE ta_area SET area_id=".$_POST['tap_aid'].",area_fte=".$_POST['tap_fte'].",area_semester='".$_POST['tap_sem']."',area_year=".$_POST['tap_year']." WHERE ta_id=$taid AND area_id=".$_POST['tap_a']);
		header("Location: ta_profile.php");
		die("Redirecting...");
	}

	if(isset($_GET['sklid']))
	{
		$sklid = $_GET['sklid'];
		//$res = $mysqlconn->query("SELECT tskl_rating FROM ta_skill WHERE ta_id=$taid AND skl_id=$sklid");
		
		try {
			$stmt = $db->prepare('SELECT tskl_rating
								  FROM ta_skill
								  WHERE ta_id-:taid AND skl_id=:sklid');
			$stmt->bindParam(':taid', $taid);
			$stmt->bindParam(':sklid', $sklid);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		$res = $stmt->fetch();
		$sklrating = isset($res['tskl_rating']) ? $res['tskl_rating'] : 1;
		$disp = 0;
	}
	
	if(isset($_GET['aid']))
	{
		$aid = $_GET['aid'];
		
		$stmt = $db->prepare('SELECT area_fte,area_semester,area_year
							  FROM ta_area
							  WHERE ta_id=:taid AND area_id=:aid');
		$stmt->bindParam(':taid', $taid);
		$stmt->bindParam(':aid', $aid);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		
		//$res = $mysqlconn->query("SELECT area_fte,area_semester,area_year FROM ta_area WHERE ta_id=$taid AND area_id=$aid");
		$res = $res->fetch();
		$afte = isset($res['area_fte']) ? $res['area_fte'] : 1;
		$asem = $res['area_semester'];
		$ayear = isset($res['area_year']) ? $res['area_year'] : 2015;
		$disp = 1;
	}
?>
<html>
	<head>
		<title>TADB - TA Profile Edit</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
	<body>
		<?php include 'includes/header.php'; ?>
		<div class="container">
		<?php
			if($disp == 0)
			{
				echo '<h2>Edit Skill</h2>';
				echo '<form name="edit_skill" action="edit_ta_profile.php" method="post">';
				echo '	<input type="hidden" name="tap_s" value="'.$sklid.'" />';
				echo '	Skill: <select name="tap_skl" class="form-control">';
								$stmt = $db->prepare("SELECT skl_id,skl_name FROM skill");
								$stmt->execute();
								$stmt->setFetchMode(PDO::FETCH_ASSOC);
								while($row = $stmt->fetch())
								{
									$selopt = '';
									if($row['skl_id'] == $sklid)
										$selopt = ' selected';
									echo "<option value=\"".$row['skl_id']."\"".$selopt.">".$row['skl_name']."</option>";
								}
				echo '		</select>';
				echo '	Rating: <input type="number" class="form-control" name="tap_rating" min="1" max="5" value="'.$sklrating.'" />';
				echo '	<input type="submit" class="btn btn-primary btn-lg" value="Edit" />';
				echo '</form>';
			}elseif($disp == 1){
				echo '<h2>Edit Area</h2>';
				echo '<form name="edit_area" action="edit_ta_profile.php" method="post">';
				echo '	<input type="hidden" name="tap_a" value="'.$aid.'" />';
				echo '	Area: <select class="form-control" name="tap_aid">';
								$res3 = $db->query("SELECT area_id,area_name FROM area ORDER BY area_name");
								while ($row = $res3->fetch_assoc())
								{
									$selopt = '';
									if($row['area_id'] == $aid)
										$selopt = ' selected';
									echo "<option value=\"".$row['area_id']."\"$selopt>".$row['area_name']."</option>";
								}
				echo '		</select>';
				echo '	FTE: <input type="number" class="form-control" name="tap_fte" min="0" max="9.99" step="0.01" value="'.$afte.'" />';
				echo '	Semester: <select class="form-control" name="tap_sem">';
				echo '		<option value="fall">Fall</option>';
				echo '		<option value="spr">Spring</option>';
				echo '		<option value="sum">Summer</option>';
				echo '	</select>';
				echo '	Year: <input type="number" class="form-control" name="tap_year" min="1990" max="2025" value="'.$ayear.'" />';
				echo '	<input type="submit" class="btn btn-primary btn-lg" value="Edit" />';
				echo '</form>';
			}
		?>
		</div>
		<?php include 'includes/footer.php'; ?>
	</body>
</html>