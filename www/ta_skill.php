<?php
	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
?>
<html>
	<head>
		<title>TADB - TA Skills</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
		<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css"/> <link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDel(id,id2)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+":"+id2+"?")
				if(opt==true)
					window.location.href = "delete.php?type=ta_skill&id="+id;
			}
		</script>
		<div class="container">
			<h1>TA Skills</h1>
			<div class="table-responsive">
			<table id="myTable" class="table" >
				<thead>
					<tr>
						<th>TA</th>
						<th>Skill</th>
						<th>Rating</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<?php
				try{
					$stat1 = $db->prepare("SELECT ta_skill.ta_id,ta_skill.skl_id,tskl_rating,ta_fname,ta_lname,skl_name FROM ta_skill NATURAL JOIN ta NATURAL JOIN skill ORDER BY ta_lname,ta_fname,tskl_rating");
				/*	$stat1->bindParam(":taSkill_taId", "ta_skill.ta_id");
					$stat1->bindParam(":taSkill_sklId", "ta_skill.skl_id");
					$stat1->bindParam(":taFname", "ta_fname");
					$stat1->bindParam(":tsklRating", "tskl_rating");
					$stat1->bindParam(":taLname", "ta_lname");
					$stat1->bindParam(":skl_name", "skl_name");
					$stat1->bindParam(":taSkill", "ta_skill");
					$stat1->bindParam(":ta", "ta");
					$stat1->bindParam(":skill", "skill");
				*/	$stat1->execute();
					//$res = $mysqlconn->query("SELECT ta_skill.ta_id,ta_skill.skl_id,tskl_rating,ta_fname,ta_lname,skl_name FROM ta_skill NATURAL JOIN ta NATURAL JOIN skill ORDER BY ta_lname,ta_fname,tskl_rating");
					$stat1->setFetchMode(PDO::FETCH_ASSOC);
				}catch (PDOException $e){
					$e->getMessage();
					sleep(10);
					die();
				}
					while($row = $stat1->fetch())
					{
						if ($row['tskl_rating'] > 5){
							$row['tskl_rating'] = 5;
						} elseif ($row['tskl_rating'] < 1){
							$row['tskl_rating'] = 1;
						}
						echo "<tr>";
						echo "<td>";
							echo htmlspecialchars($row['ta_lname']) . ', ' . htmlspecialchars($row['ta_fname']);
						echo "</td>";
						echo "<td>";
							echo ucfirst(htmlspecialchars($row['skl_name']));
						echo "</td>";
						echo "<td>";
							echo htmlspecialchars($row['tskl_rating']);
						echo "</td>";
						echo "<td>";
							echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_ta_skill.php?taid=".$row['ta_id']."&sklid=".$row['skl_id']."\">Edit</a>";
						echo "</td>";
						echo "<td>";
							echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDel(".$row['ta_id'].",".$row['skl_id'].")\">Delete</button>";
						echo "</td>";
						echo "</tr>";
					}
				?>
			</table>
			</div>
			<hr />
			<h2>Add TA Skill</h2>
			<form name="add_taskill" action="add.php" method="post">
				<input type="hidden" name="type" value="ta_skill" />
				TA: <select class="form-control" name="tas_ta">
								<?php
									$stat2 = $db->prepare("SELECT ta_id,ta_fname,ta_lname FROM ta ORDER BY ta_lname,ta_fname");
								/*	$stat2->bindParam(":taFname", "ta_fname");
									$stat2->bindParam(":ta", "ta");
									$stat2->bindParam(":taLname", "ta_lname");
									$stat2->bindParam(":taId", "ta_id");
								*/	$stat2->execute();
									//$res2 = $mysqlconn->query("SELECT ta_id,ta_fname,ta_lname FROM ta ORDER BY ta_lname,ta_fname");
									while($row = $stat2->fetch(PDO::FETCH_ASSOC))
										echo "<option value=\"".$row['ta_id']."\">".$row['ta_fname']." ".$row['ta_lname']."</option>";
								?>
							</select>
				Skill: <select class="form-control" name="tas_skl">
								<?php
									$stat3 = $db->prepare("SELECT skl_id,skl_name FROM skill");
								/*	$stat3->bindParam(":sklId", "skl_id");
									$stat3->bindParam(":sklName", "skl_name");
									$stat3->bindParam(":skill", "skill");
								*/	$stat3->execute();
									//$res3 = $mysqlconn->query("SELECT skl_id,skl_name FROM skill");
									while($row = $stat3->fetch(PDO::FETCH_ASSOC))
										echo "<option value=\"".$row['skl_id']."\">".$row['skl_name']."</option>";
								?>
							</select>
				Rating: 
				<select class="form-control" name="tas_rating">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>
				<input type="submit" class="btn btn-primary btn-lg" value="Add" />
			</form>
		</div>
		<?php 
		include 'includes/footer.php';
        $db = null;
		?>
	</body>
	<script>
		$(document).ready(function(){
			$('#myTable').dataTable({
				"columns": [
					null,
					null,
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
		});
	</script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>
</html>