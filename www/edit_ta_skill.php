<?php
//TODO
	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	
	$tid = $sid = $tsrating = 0;

	if(!empty($_POST['ts_taid']) && !empty($_POST['ts_sid']))
	{
		
		try {
			$stmt = $db->prepare('UPDATE ta_skill
								  SET tskl_rating=:ts_rating, skl_id=:tas_skl, ta_id=:tas_ta
								  WHERE ta_id=:taid AND skl_id=:sklid');
			$stmt->bindParam(':taid', $_POST['ts_taid']);
			$stmt->bindParam(':sklid', $_POST['ts_sid']);
			$stmt->bindParam(':ts_rating', $_POST['ts_rating']);
			$stmt->bindParam(':tas_skl', $_POST['tas_skl']);
			$stmt->bindParam(':tas_ta', $_POST['tas_ta']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}

		header("Location: ta_skill.php");
		die('Redirecting...');
		
	}elseif(isset($_GET['taid']) && isset($_GET['sklid'])){
		$tid = $_GET['taid'];
		$sid = $_GET['sklid'];
		
		try {
			$stmt = $db->prepare('SELECT tskl_rating 
								  FROM ta_skill
								  WHERE ta_id=:tid AND skl_id=$:sid');
			$stmt->bindParam(':tid', $tid);
			$stmt->bindParam(':sid', $sid);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$res = $stmt->fetch();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		//$res = $mysqlconn->query("SELECT tskl_rating FROM ta_skill WHERE ta_id=$tid AND skl_id=$sid");
		//$res = $res->fetch_assoc();
		$tsrating = isset($res['tskl_rating']) ? $res['tskl_rating'] : 1;
	}
	else
		die("No ID provided");
?>
<html>
	<head>
		<title>TADB - TA-Skill Edit</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
	<body>
		<?php include 'includes/header.php'; ?>
		<div class="container">
			<h2>Edit TA Skill</h2>
			<form name="edit" action="edit_ta_skill.php" method="post">
				<input type="hidden" name="ts_taid" value="<?php echo $tid; ?>" />
				<input type="hidden" name="ts_sid" value="<?php echo $sid; ?>" />
				TA: <select name="tas_ta" class="form-control">
						<?php
							$res2 = $db->query("SELECT ta_id,ta_fname,ta_lname FROM ta ORDER BY ta_lname,ta_fname");
							$res2->setFetchMode(PDO::FETCH_ASSOC);
							while($row = $res2->fetch())
							{
								$selopt = '';
								if($row['ta_id'] == $tid)
									$selopt = ' selected';
								echo "<option value=\"".$row['ta_id']."\"" . $selopt . ">".$row['ta_fname']." ".$row['ta_lname']."</option>";
							}
						?>
					</select>
				Skill: <select name="tas_skl" class="form-control">
						<?php
							$res3 = $db->query("SELECT skl_id,skl_name FROM skill");
							$res3->setFetchMode(PDO::FETCH_ASSOC);
							while($row = $res3->fetch())
							{
								$selopt = '';
								if($row['skl_id'] == $sid)
									$selopt = ' selected';
								echo "<option value=\"".$row['skl_id']."\"".$selopt.">".$row['skl_name']."</option>";
							}
						?>
					</select>
				Rating: 
				<select class="form-control" name="tas_rating">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>
				<input type="submit" class="btn btn-primary btn-lg" value="Edit" />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
		<?php $db = null; ?>
	</body>
</html>