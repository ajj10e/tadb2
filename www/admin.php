<?php
	include_once('includes/db.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	include_once "./includes/connection.php";
	//include_once('change_role.php');
?>
<html>
	<head>
		<title>TADB - Admin</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css"/> <link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDel(id)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+"?")
				if(opt==true)
					window.location.href = "delete.php?type=user&id="+id;
			}
		</script>
		<div class="container">
			<h1>Admin</h1>
			<div class="table-responsive">
			<table id="myTable" class="table" >
				<thead>
					<tr>
						<th>User</th>
						<th>Name</th>
						<th>Role</th>
						<th></th>
						<th>Status</th>
						<th></th>
						<th>Delete</th>
					</tr>
				</thead>
				<?php
					//$res = $mysqlconn->query("SELECT crs_id,crs_num,crs_name,crs_description FROM course ORDER BY crs_name");
					try {
						$stmt = $db->prepare('SELECT usr_id, usr_email, usr_lname, usr_fname, usr_role, usr_status 
											  FROM user 
											  ORDER BY usr_role, usr_lname, usr_fname;');
						$stmt->execute();
						$stmt->setFetchMode(PDO::FETCH_ASSOC);
						
						while($row = $stmt->fetch())
						{
							if ($_SESSION['email'] == $row['usr_email']) {
								$name = $row['usr_lname'].", ".$row['usr_fname'];
								echo "<tr>";
								echo "<td>";
								echo htmlspecialchars($row['usr_email']);
								echo "</td>";
								echo "<td>";
								echo htmlspecialchars($name);
								echo "</td>";
								echo "<td>";
								echo htmlspecialchars($row['usr_role']);
								echo "</td>";
								echo "<td>";
								$uid = $row['usr_id'];
								$role = $row['usr_role'];
								echo "<a class=\"btn btn-warning btn-xs btn-block disabled\" href=\"#\">Toggle Role</a>";
								echo "</td>";
								echo "<td>";
								echo htmlspecialchars($row['usr_status']);
								echo "</td>";
								echo "<td>";
								echo "<a class=\"btn btn-warning btn-xs btn-block disabled\" href=\"#\">Toggle Status</a>";
								echo "</td>";
								echo "<td>";
								echo "<button class=\"btn btn-danger btn-xs btn-block disabled\" href=\"#\">Delete User</a>";
								echo "</td>";
								echo "</tr>";
							}
							else {
								$name = $row['usr_lname'].", ".$row['usr_fname'];
								echo "<tr>";
								echo "<td>";
								echo htmlspecialchars($row['usr_email']);
								echo "</td>";
								echo "<td>";
								echo htmlspecialchars($name);
								echo "</td>";
								echo "<td>";
								echo htmlspecialchars($row['usr_role']);
								echo "</td>";
								echo "<td>";
								$uid = $row['usr_id'];
								$role = $row['usr_role'];
								echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"update_user.php?id=$uid&attr=role&val=$role\">Toggle Role</a>";
								echo "</td>";
								echo "<td>";
								echo htmlspecialchars($row['usr_status']);
								echo "</td>";
								echo "<td>";
								$status = $row['usr_status'];
								echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"update_user.php?id=$uid&attr=status&val=$status\">Toggle Status</a>";
								echo "</td>";
								echo "<td>";
								echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDel($uid);\">Delete User</a>";
								echo "</td>";
								echo "</tr>";
							}
						}
					}
					catch(PDOException $e) {
						include_once('includes/error.php');
					}
				?>
			</table>
			</div>
			<hr />
		</div>
		<?php include 'includes/footer.php'; ?>
	</body>
	<script>
		$(document).ready(function(){
			$('#myTable').dataTable({
				"columns": [
					null,
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
		});
	</script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>
</html>