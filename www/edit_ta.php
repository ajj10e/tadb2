<?php
//TODO

	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	
	$tid = NULL;
	$tfn = $tln = $te = $tti = $tri = '';

	if(!empty($_POST['t_id']))
	{
		//what should we actually do to check for this whole mess?
		if(!isset($_POST['t_email']) || !(preg_match("/([A-Za-z0-9]+)@([A-Za-z0-9]*)(\.?)fsu\.edu/",$_POST['t_email'])))
			die("Invalid E-mail");

		//$mysqlconn->query("UPDATE ta SET ta_fname='".$_POST['t_fname']."',ta_lname='".$_POST['t_lname']."',ta_email='".$_POST['t_email']."',ta_teaching_interests='".$_POST['t_t_i']."',ta_research_interests='".$_POST['t_r_i']."' WHERE ta_id=".$_POST['t_id']);
		
		try {
			$stmt = $db->prepare('UPDATE ta
								  SET ta_fname=:t_fname, 
								  	  ta_lname=:t_lname, 
								  	  ta_email=:t_email, 
								  	  ta_teaching_interests=:t_t_i, 
								  	  ta_research_interests=:t_r_i
								  WHERE ta_id=:t_id');
			$stmt->bindParam(':t_fname', $_POST['t_fname']);
			$stmt->bindParam(':t_lname', $_POST['t_lname']);
			$stmt->bindParam(':t_email', $_POST['t_email']);
			$stmt->bindParam(':t_t_i', $_POST['t_t_i']);
			$stmt->bindParam(':t_r_i', $_POST['t_r_i']);
			$stmt->bindParam(':t_id', $_POST['t_id']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		header("Location: ta.php");
		die('Redirecting...');
	}elseif(isset($_GET['id'])){
		$tid = $_GET['id'];
		
		try {
			$stmt = $db->prepare('SELECT ta_fname,ta_lname,ta_email,ta_teaching_interests,ta_research_interests
								  FROM ta
								  WHERE ta_id=:tid');
			$stmt->bindParam(':tid', $tid);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$res = $stmt->fetch();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		$tfn = isset($res['ta_fname']) ? $res['ta_fname'] : '';
		$tln = isset($res['ta_lname']) ? $res['ta_lname'] : '';
		$te = isset($res['ta_email']) ? $res['ta_email'] : '';
		$tti = isset($res['ta_teaching_interests']) ? $res['ta_teaching_interests'] : '';
		$tri = isset($res['ta_research_interests']) ? $res['ta_research_interests'] : '';
	}
	else
		die("No ID provided");
?>
<html>
	<head>
		<title>TADB - TA Edit</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
	<body>
		<?php include 'includes/header.php'; ?>
		<div class="container">
			<h2>Edit TA</h2>
			<form name="edit_ta" action="edit_ta.php" method="post">
				<input type="hidden" name="t_id" value="<?php echo $tid; ?>" />
				First name: <input type="text" class="form-control" name="t_fname" value="<?php echo $tfn; ?>" />
				Last name: <input type="text" class="form-control" name="t_lname" value="<?php echo $tln; ?>" />
				E-mail: <input type="email" class="form-control" name="t_email" value="<?php echo $te; ?>" />
				Teaching interests: <input type="text" class="form-control" name="t_t_i" value="<?php echo $tti; ?>" />
				Research interests: <input type="text" class="form-control" name="t_r_i" value="<?php echo $tri; ?>" />
				<input type="submit" class="btn btn-primary btn-lg" value="Edit" />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
	</body>
</html>