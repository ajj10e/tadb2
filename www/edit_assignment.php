<?php

	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	
	$tid = $fid = $cid = $sem = $year = $reason = 0;
	
	//If a_tid, a_fid, and a_cid are not empty, update the record.
	//If one or more are empty, but tid, fid, and cid are not, retrieve all area names and store in res.
	//Otherwise, die.
	if(!empty($_POST['a_tid_']) && !empty($_POST['a_fid_']) && !empty($_POST['a_cid_']))
	{
		try {
			$stmt = $db->prepare('UPDATE assignment 
								  SET ta_id=:a_tid, fac_id=:a_fid, crs_id=:a_cid, asn_semester=:a_sem, asn_year=:a_year, asn_reason=:a_reason 
								  WHERE ta_id=:a_tid_ AND fac_id=:a_fid_ AND crs_id=:a_cid_');
			$stmt->bindParam(':a_tid', 		$_POST['a_tid']);
			$stmt->bindParam(':a_fid', 		$_POST['a_fid']);
			$stmt->bindParam(':a_cid', 		$_POST['a_cid']);
			$stmt->bindParam(':a_sem', 		$_POST['a_sem']);
			$stmt->bindParam(':a_year', 	$_POST['a_year']);
			$stmt->bindParam(':a_reason', 	$_POST['a_reason']);
			$stmt->bindParam(':a_tid_', 	$_POST['a_tid_']);
			$stmt->bindParam(':a_fid_', 	$_POST['a_fid_']);
			$stmt->bindParam(':a_cid_', 	$_POST['a_cid_']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		header("Location: assignment.php");
		die('Redirecting...');
	}
	elseif(!empty($_GET['tid']) && !empty($_GET['fid']) && !empty($_GET['cid'])){
		$tid = $_GET['tid'];
		$fid = $_GET['fid'];
		$cid = $_GET['cid'];
		try {
			$stmt = $db->prepare('SELECT asn_semester,asn_year,asn_reason 
								  FROM assignment 
								  WHERE ta_id=:ta_id AND fac_id=:fac_id AND crs_id=:crs_id');
			$stmt->bindParam(':ta_id', $tid);
			$stmt->bindParam(':crs_id', $cid);
			$stmt->bindParam(':fac_id', $fid);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$res = $stmt->fetch();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		$sem = $res['asn_semester'];
		$year = $res['asn_year'];
		$reason = $res['asn_reason'];
	}
	else
		die("No ID provided");
?>
<html>
	<head>
		<title>TADB - Assignment Edit</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
	<body>
		<?php include 'includes/header.php'; ?>
		<div class="container">
			<h2>Edit Assignment</h2>
			<form name="edit" action="edit_assignment.php" method="post">
				<input type="hidden" name="a_tid_" value="<?php echo $tid; ?>" />
				<input type="hidden" name="a_fid_" value="<?php echo $fid; ?>" />
				<input type="hidden" name="a_cid_" value="<?php echo $cid; ?>" />
				TA: <select class="form-control" name="a_tid">
								<?php
									$res3 = $db->query("SELECT ta_id,CONCAT(ta_lname,', ',ta_fname) AS ta_name FROM ta ORDER BY ta_lname,ta_fname");
									$res3->setFetchMode(PDO::FETCH_ASSOC);
									while($row = $res3->fetch())
									{
										$selopt = '';
										if($row['ta_id'] == $tid)
											$selopt = ' selected';
										echo "<option value=\"".$row['ta_id']."\"" . $selopt . ">".$row['ta_name']."</option>";
									}
								?>
							</select>
				Course: <select class="form-control" name="a_cid">
								<?php
									$res2 = $db->query("SELECT crs_id,crs_name FROM course ORDER BY crs_name");
									$res2->setFetchMode(PDO::FETCH_ASSOC);
									while($row = $res2->fetch())
									{
										$selopt = '';
										if($row['crs_id'] == $cid)
											$selopt = ' selected';
										echo "<option value=\"".$row['crs_id']."\"" . $selopt . ">".$row['crs_name']."</option>";
									}
								?>
							</select>
				Faculty: <select class="form-control" name="a_fid">
								<?php
									$res3 = $db->query("SELECT fac_id,CONCAT(fac_lname,', ',fac_fname) AS fac_name FROM faculty ORDER BY fac_lname,fac_fname");
									$res3->setFetchMode(PDO::FETCH_ASSOC);
									while($row = $res3->fetch())
									{
										$selopt = '';
										if($row['fac_id'] == $fid)
											$selopt = ' selected';
										echo "<option value=\"".$row['fac_id']."\"" . $selopt . ">".$row['fac_name']."</option>";
									}
								?>
							</select>
				Semester: <select class="form-control" name="a_sem">
								<option value="fall">Fall</option>
								<option value="spr">Spring</option>
								<option value="sum">Summer</option>
							</select>
				Year: 
				<select class="form-control" name="a_year">
					<option value="2000">2000</option>
					<option value="2001">2001</option>
					<option value="2002">2002</option>
					<option value="2003">2003</option>
					<option value="2004">2004</option>
					<option value="2005">2005</option>
					<option value="2006">2006</option>
					<option value="2007">2007</option>
					<option value="2008">2008</option>
					<option value="2009">2009</option>
					<option value="2010">2010</option>
					<option value="2011">2011</option>
					<option value="2012">2012</option>
					<option value="2013">2013</option>
					<option value="2014">2014</option>
					<option value="2015">2015</option>
					<option value="2016">2016</option>
					<option value="2017">2017</option>
					<option value="2018">2018</option>
					<option value="2019">2019</option>
					<option value="2020">2020</option>
				</select>
				Notes: <input type="text" class="form-control" name="a_reason" value="<?php echo $reason; ?>"/>
				<input type="submit" class="btn btn-primary btn-lg" value="Edit" />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
		<?php $db = null; ?>
	</body>
</html>