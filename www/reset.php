<?php
	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
?>
<html>
	<head>
		<title>TADB - Resets</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDel(id)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+"?")
				if(opt==true)
					window.location.href = "delete.php?type=area&id="+id;
			}
		</script>
		<?php include_once('includes/reset.php'); ?>
		<?php 
		include 'includes/footer.php'; 
		$db = null;
		?>
	</body>
	<script>
		$(document).ready(function(){
			$('#myTable').dataTable({
				"columns": [
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
		});
	</script>
</html>