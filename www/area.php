<?php

	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
?>
<html>
	<head>
		<title>TADB - Areas</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
		<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css"/> <link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDel(id)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+"?")
				if(opt==true)
					window.location.href = "delete.php?type=area&id="+id;
			}
		</script>
		<div class="container">
			<h1>Areas</h1>
			<div class="table-responsive">
			<table id="myTable" class="table" >
				<thead>
					<tr>
						<th>Area Name</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<?php
				try{
					$statement = $db->prepare("SELECT area_id,area_name FROM area ORDER BY area_name");
					$statement->execute();
					$statement->setFetchMode(PDO::FETCH_ASSOC);
					//$res = $mysqlconn->query("SELECT area_id,area_name FROM area ORDER BY area_name");
				}catch(PDOException $e){
					echo $e->getMessage();
					sleep(10);
					die();
				}
					while($row = $statement->fetch())
					{
						echo "<tr>";
						echo "<td>";
							echo htmlspecialchars($row['area_name']);
						echo "</td>";
						echo "<td>";
							echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_area.php?id=".$row['area_id']."\">Edit</a>";
						echo "</td>";
						echo "<td>";
							echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDel(".$row['area_id'].")\">Delete</button>";
						echo "</td>";
						echo "</tr>";
					}
				?>
			</table>
			</div>
			<hr />
			<h2>Add Area</h2>
			<form name="add_area" action="add.php" method="post">
				<input type="hidden" name="type" value="area" />
				Area Name: <input type="text" class="form-control" name="a_name" />
				<input type="submit" class="btn btn-primary btn-lg" value="Add" />
			</form>
		</div>
		<?php 
		include 'includes/footer.php'; 
		$db = null;
		?>
	</body>
	<script>
		$(document).ready(function(){
			$('#myTable').dataTable({
				"columns": [
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
		});
	</script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>

</html>