<?php
	include_once('includes/config.php');
	include_once "includes/connection.php";
	if(isset($_POST['navtarget']))
		header("Location: ".$_POST['navtarget'].".php");
?>
<html>
	<head>
		<title>TADB</title>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<div class="container" style="width:40%">
		<?php
		if(isset($_SESSION["rank"])) {
		
			$rank = $_SESSION['rank'];
			if($rank == $ranks['admin']) {
				header("Location: assignment.php");
			}
			elseif($rank == $ranks['ta']) {
				header("Location: ta_profile.php");
			}
		}
		else if(!empty($_GET['r'])) {
		
			$link = $_GET['r'];
			
			echo '
			<div class="panel panel-primary">
				<div class="panel-heading">Reset Password</div>
				<div class="panel-body">
					<form role="form" name="reset" action="login.php?r='.$link.'" method="post">
						<div class="form-group">
							<label for="pass">New Password</label>
							<input type="password" class="form-control" id="pass" name="pass" placeholder="New Password">
						</div>
						<div class="form-group">
							<label for="confirm">Confirm Password</label>
							<input type="password" class="form-control" id="confirm" name="confirm" placeholder="Confirm Password">
						</div>
						<input class="btn btn-success btn-lg" type="submit" value="Reset Password">
					</form>
				</div>
			</div>';
		}
		else {
			echo '
			<div class="panel panel-primary">
				<div class="login panel-heading">Login</div>
				<div class="sign-up panel-heading">Sign Up</div>
				<div class="panel-body">
					<form role="form" name="login" action="login.php" method="post">
						<div class="form-group sign-up">
							<label for="fname">First Name</label>
							<input type="text" class="form-control" id="fname" name="fname" placeholder="First Name">
						</div>
						<div class="form-group sign-up">
							<label for="lname">Last Name</label>
							<input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name">
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" class="form-control" id="email" name="email" placeholder="FSU Email">
						</div>
						<div class="pw form-group">
							<label for="pass">Password</label>
							<input type="password" class="form-control" id="pass" name="pass" placeholder="Password">
						</div>
						<div class="pw form-group sign-up">
							<label for="confirm">Confirm Password</label>
							<input type="password" class="form-control" id="confirm" name="confirm" placeholder="Confirm Password">
						</div>
						<input class="btn btn-success btn-lg login" type="submit" value="Login">
						<input class="btn btn-success btn-lg sign-up" type="submit" value="Sign Up">
						<input class="btn btn-success btn-lg reset" type="submit" value="Reset Password">
					</form>
					<div class="login">Don\'t have an account yet? <a class="toggle" href="#signup">Sign Up</a></div>
					<div class="sign-up">Already have an account? <a class="toggle" href="#login">Login</a></div>
					<div class="login">Forgot your password? <a class="login reset" href="#reset">Reset</a></div>
					<a class="reset" href="index.php">Back</a>
				</div>
			</div>';
		}
		?>
		</div>
		<?php include 'includes/footer.php'; ?>
		<script type="text/javascript">
		
		$(".sign-up").hide();
		$(".reset").hide();
		$(".login").show();
		
		$(".toggle").click(function(){
    		$(".sign-up").toggle();
    		$(".login").toggle();
		});
		
		$(".reset").click(function(){
    		$(".sign-up").hide();
    		$(".login").hide();
    		$(".pw").hide();
    		$(".reset").show();
		});
		
		</script>
	</body>
</html>