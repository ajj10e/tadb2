<?php

	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	
	//Default values
	$cid = 1;
	$taid = 1;
	$fid = 1;
	
	//Check for get parameters
	//If found, set cid, taid, and fid to populate the form
	if(!empty($_GET['cid']) && !empty($_GET['taid']) && !empty($_GET['fid'])){
		$cid = $_GET['cid'];
		$taid = $_GET['taid'];
		$fid = $_GET['fid'];
	}	

?>
<html>
	<head>
		<title>TADB - Assignments</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
		<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css"/> <link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDel(id,id2,id3)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+":"+id2+":"+id3+"?")
				if(opt==true)
					window.location.href = "delete.php?type=assignment&id="+id+"&id2="+id2+"&id3="+id3;
			}
		</script>
		<div class="container">
			<h1>Assignments</h1>
			<div class="table-responsive">
			<table id="myTable" class="table" >
				<thead>
					<tr>
						<th>TA</th>
						<th>Faculty</th>
						<th>Course</th>
						<th>Semester</th>
						<th>Year</th>
						<th>Notes</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<?php
				try{
					$statement = $db->prepare("SELECT ta_id,CONCAT(ta_lname,', ',ta_fname) AS ta_name,fac_id,CONCAT(fac_lname,', ',fac_fname) AS fac_name,crs_id,crs_name,asn_semester,asn_year,asn_reason FROM assignment NATURAL JOIN ta NATURAL JOIN faculty NATURAL JOIN course ORDER BY crs_name,fac_lname,fac_fname,ta_lname,ta_fname");
					$statement->execute();
					$statement->setFetchMode(PDO::FETCH_ASSOC);
				}catch(PDOException $e){
					echo $e->getMessage();
					sleep(10);
					die();
				}
					while($row = $statement->fetch())
					{
						echo "<tr>";
						echo "<td>";
							echo htmlspecialchars($row['ta_name']);
						echo "</td>";
						echo "<td>";
							echo htmlspecialchars($row['fac_name']);
						echo "</td>";
						echo "<td>";
							echo htmlspecialchars($row['crs_name']);
						echo "</td>";
						echo "<td>";
							$semester = htmlspecialchars($row['asn_semester']);
							if ($semester === "spr"){
								echo "Spring";
							}else if ($semester === "fall"){
								echo "Fall";
							}else{
								echo "Summer";
							}
						echo "</td>";
						echo "<td>";
							echo htmlspecialchars($row['asn_year']);
						echo "</td>";
						echo "<td>";
							echo htmlspecialchars($row['asn_reason']);
						echo "</td>";
						echo "<td>";
							echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_assignment.php?cid=".$row['crs_id']."&tid=".$row['ta_id']."&fid=".$row['fac_id']."\">Edit</a>";
						echo "</td>";
						echo "<td>";
							echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDel(".$row['ta_id'].",".$row['fac_id'].",".$row['crs_id'].")\">Delete</button>";
						echo "</td>";
						echo "</tr>";
					}
				?>
			</table>
			</div>
			<hr />
			<a name="add"></a>
			<h2>Add Assignment</h2>
			<form name="add_assignment" action="add.php" method="post">
				<input type="hidden" name="type" value="assignment" />
				TA: <select class="form-control" name="a_tid">
								<?php
								/*
									$res3 = $mysqlconn->query("SELECT ta_id,CONCAT(ta_lname,', ',ta_fname) AS ta_name FROM ta ORDER BY ta_lname,ta_fname");
									while($row = $res3->fetch_assoc()){
									*/
									$stmt = $db->prepare("SELECT ta_id,CONCAT(ta_lname,', ',ta_fname) AS ta_name FROM ta ORDER BY ta_lname,ta_fname");
									
									$stmt->execute();
									$stmt->setFetchMode(PDO::FETCH_ASSOC);
									while($row = $stmt->fetch()){
									
										//Check for get parameters to populate form
										if($row['ta_id'] == $taid)
											echo "<option value=\"".$row['ta_id']."\" selected >".$row['ta_name']."</option>";
										else
											echo "<option value=\"".$row['ta_id']."\">".$row['ta_name']."</option>";
									}
								?>
							</select>
				Course: <select class="form-control" name="a_cid">
								<?php
									/*
									$res2 = $mysqlconn->query("SELECT crs_id,crs_num,crs_name FROM course ORDER BY crs_num ASC");
									while($row = $res2->fetch_assoc()){
									*/
									$stmt = $db->prepare("SELECT crs_id,crs_num,crs_name FROM course ORDER BY crs_num ASC");
									
									$stmt->execute();
									$stmt->setFetchMode(PDO::FETCH_ASSOC);
									while($row = $stmt->fetch()){
									
										//Check for get parameters to populate form
										if($row['crs_id'] == $cid)
											echo "<option value=\"".$row['crs_id']."\" selected >".$row['crs_num']." - ".$row['crs_name']."</option>";
										else
											echo "<option value=\"".$row['crs_id']."\">".$row['crs_num']." - ".$row['crs_name']."</option>";
									}
								?>
							</select>
				Faculty: <select class="form-control" name="a_fid">
								<?php
									$stmt = $db->prepare("SELECT fac_id,CONCAT(fac_lname,', ',fac_fname) AS fac_name FROM faculty ORDER BY fac_lname,fac_fname");
									
									$stmt->execute();
									$stmt->setFetchMode(PDO::FETCH_ASSOC);
									while($row = $stmt->fetch()){
										
										if($row['fac_id'] == $fid)
											echo "<option value=\"".$row['fac_id']."\" selected >".$row['fac_name']."</option>";
										else
											echo "<option value=\"".$row['fac_id']."\">".$row['fac_name']."</option>";
									}
								?>
							</select>
				Semester: <select class="form-control" name="a_sem">
								<option value="fall">Fall</option>
								<option value="spr">Spring</option>
								<option value="sum">Summer</option>
							</select>
				Year: 
				<select class="form-control" name="a_year">
					<option value="2000">2000</option>
					<option value="2001">2001</option>
					<option value="2002">2002</option>
					<option value="2003">2003</option>
					<option value="2004">2004</option>
					<option value="2005">2005</option>
					<option value="2006">2006</option>
					<option value="2007">2007</option>
					<option value="2008">2008</option>
					<option value="2009">2009</option>
					<option value="2010">2010</option>
					<option value="2011">2011</option>
					<option value="2012">2012</option>
					<option value="2013">2013</option>
					<option value="2014">2014</option>
					<option value="2015">2015</option>
					<option value="2016">2016</option>
					<option value="2017">2017</option>
					<option value="2018">2018</option>
					<option value="2019">2019</option>
					<option value="2020">2020</option>
				</select>
				Notes: <input type="text" class="form-control" name="a_reason" />
				<input type="submit" class="btn btn-primary btn-lg" value="Add" />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
	</body>
	<script>
		$(document).ready(function(){
			$('#myTable').dataTable({
				"columns": [
					null,
					null,
					null,
					null,
					null,
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
		});
	</script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>
	<?php
		$db = null;
	?>
</html>