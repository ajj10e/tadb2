<?php

	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	
	function changeRole(uid) {
		
	}
		
	if (isset($_GET['changeType'])) {
		
	
		if ($_GET['changeType'] === 'status'){
			$stat1;
		}else{ 
			$userId = $_GET['id'];
			$stat3 = $db->prepare("SELECT FROM user WHERE usr_id=$userId LIMIT 1;");
			$stat3->execute();
			$stat3->setFetchMode(PDO::FETCH_ASSOC);
			while ($row = $stat3->fetch()){
				$userStatus = htmlspecialchars($row['usr_status']);
			}
			$setTo = '';
			$stat4 = $db->prepare("UPDATE user SET usr_status=$setTo WHERE usr_id=$userId;");
			if ($userStatus === 'Pending'){
				$setTo = 'Approved';
			}else if ($userStatus === 'Approved'){
				$setTo = 'Declined';
			}else{
				$setTo = 'Pending';
			}
			$stat4->execute();
			$db = null;
			header('Location: admin.php');
		}
	}
	else {
		die("changeType not set");
	}
	
	if (empty($_GET['id'])){
		header("Location: ta.php");
		die('No such user found.');
	}else{
		$stat1 = $db->prepare("SELECT usr_role FROM user WHERE usr_id=:usrid LIMIT 1;");
		$stat1->bindParam(':usrid', $_GET['usr_id']);
		//$stat1 = $db->prepare("SELECT adm_email, adm_role, adm_status FROM admin ORDER BY adm_role DESC, adm_status DESC, adm_email DESC;");
		$stat1->execute();
		$stat1->setFetchMode(PDO::FETCH_ASSOC);
		while ($row = $stat1->fetch()){
			$userRole = htmlspecialchars($row['usr_role']);
		}
		if ($userRole === 'Admin'){
			$stat2 = $db->prepare("UPDATE user SET usr_role='TA' WHERE usr_id=:theid;");
			$stat2->bindParam(':theid', $_GET['id']);
		}else{
			$stat2 = $db->prepare("UPDATE user SET usr_role='Admin' WHERE usr_id=:theid;");
			$stat2->bindParam(':theid', $_GET['id']);
		}
		$stat2->execute();
		$db = null;
		header("Location: admin.php");
	} 
?>