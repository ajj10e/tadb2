<?php

	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
?>
<html>
	<head>
		<title>TADB - Courses</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
		<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css"/> <link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDel(id)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+"?")
				if(opt==true)
					window.location.href = "delete.php?type=course&id="+id;
			}
		</script>
		<div class="container">
			<h1>Courses</h1>
			
			<div class="table-responsive">

			<table id="myTable" class="table">
				<thead>
					<tr>
						<th>Course #</th>
						<th>Course Name</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
				<?php
					//$res = $mysqlconn->query("SELECT crs_id,crs_num,crs_name,crs_description FROM course ORDER BY crs_name");
					try {
						$stat1 = $db->prepare("SELECT crs_id,crs_num,crs_name,crs_description FROM course ORDER BY crs_name");
						$stat1->execute();
						$stat1->setFetchMode(PDO::FETCH_ASSOC);
					} catch (PDOException $e){
						$e->getMessage();
						sleep(10);
						die();
					}

						while($row = $stat1->fetch())
						{
							echo "<tr>";
							echo "<td>";
								echo htmlspecialchars($row['crs_num']);
							echo "</td>";
							echo "<td>";
								echo htmlspecialchars($row['crs_name']);
							echo "</td>";
							echo "<td>";
								echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_course.php?id=".$row['crs_id']."\">Edit</a>";
							echo "</td>";
							echo "<td>";
								echo "<button class='btn btn-danger btn-xs btn-block' onclick='confirmDel(".$row['crs_id'].")'>Delete</button>";
							echo "</td>";
							echo "</tr>";
						}
				?>
				</tbody>
			</table>
			</div>
			<hr />
			<h2>Add Course</h2>
			<form name="add_course" action="add.php" method="post">
				<input type="hidden" name="type" value="course" />
				Course Number: <input type="text" class="form-control" name="c_num" />
				Course Name: <input type="text" class="form-control" name="c_name" />
				Description: <textarea class="form-control" rows="3" name="c_desc"></textarea>
				<input type="submit" class="btn btn-primary btn-lg" value="Add" />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
	</body>
	<script>
		$(document).ready(function(){
			$('#myTable').dataTable({
				"columns": [
					null,
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
		});
	</script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>
</html>