<?php

	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	
	//If post parameters are not empty, update the record.
	//If one or more are empty, but get parameters are not, retrieve all area names and store in res.
	//Otherwise, die.
	if(!empty($_POST['f_id']))
	{
		//what should we actually do to check for this whole mess?
		if(!isset($_POST['f_email']) || !(preg_match("/([A-Za-z0-9]+)@([A-Za-z0-9]*)(\.?)fsu\.edu/",$_POST['f_email'])))
			die("Invalid E-mail");

		try{
			$stmt = $db->prepare('UPDATE faculty 
								  SET fac_fname=:f_fname,fac_lname=:f_lname,fac_email=:f_email 
								  WHERE fac_id=:f_id');
			$stmt->bindParam(':f_fname', 	$_POST['f_fname']);
			$stmt->bindParam(':f_lname', 	$_POST['f_lname']);
			$stmt->bindParam(':f_email', 	$_POST['f_email']);
			$stmt->bindParam(':f_id', 		$_POST['f_id']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		
		header("Location: faculty.php");
		die('Redirecting...');
	}elseif(!empty($_GET['id'])){
		$fid = $_GET['id'];
		
		try{
			$stmt = $db->prepare('SELECT fac_fname,fac_lname,fac_email
								  FROM faculty
								  WHERE fac_id=:fid');
			$stmt->bindParam(':fid', $fid);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$res = $stmt->fetch();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		$ffn = isset($res['fac_fname']) ? $res['fac_fname'] : '';
		$fln = isset($res['fac_lname']) ? $res['fac_lname'] : '';
		$fe = isset($res['fac_email']) ? $res['fac_email'] : '';
	}
	else
		die("No ID provided");
?>
<html>
	<head>
		<title>TADB - Course Edit</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
	<body>
		<?php include 'includes/header.php'; ?>
		<div class="container">
			<h2>Edit Faculty</h2>
			<form name="add_faculty" action="edit_faculty.php" method="post">
				<input type="hidden" name="f_id" value="<?php echo $fid; ?>" />
				First name: <input type="text" class="form-control" name="f_fname" value="<?php echo $ffn; ?>" />
				Last name: <input type="text" class="form-control" name="f_lname" value="<?php echo $fln; ?>" />
				E-mail: <input type="email" class="form-control" name="f_email" value="<?php echo $fe; ?>" />
				<input type="submit" class="btn btn-primary btn-lg" value="Edit" />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
		<?php $db = null; ?>
	</body>
</html>