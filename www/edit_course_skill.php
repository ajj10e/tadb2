<?php

	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	
	$cid = $sid = $fid = 0;
	
	//If post parameters are not empty, update the record.
	//If one or more are empty, but get parameters are not, retrieve all area names and store in res.
	//Otherwise, die.
	if(!empty($_POST['cs_crs']) && !empty($_POST['cs_fac']) && !empty($_POST['cs_skl']))
	{
		//what should we actually do to check for this whole mess?

		try {
			$stmt = $db->prepare('UPDATE course_skill 
								  SET fac_id=:cs_fac, crs_id=:cs_crs, skl_id=:cs_skl
								  WHERE crs_id=:cs_cid AND skl_id=:cs_sid AND fac_id=:cs_fid');
			$stmt->bindParam(':cs_fac',		$_POST['cs_fac']);
			$stmt->bindParam(':cs_crs',		$_POST['cs_crs']);
			$stmt->bindParam(':cs_skl',		$_POST['cs_skl']);
			$stmt->bindParam(':cs_cid',		$_POST['cs_cid']);
			$stmt->bindParam(':cs_sid',		$_POST['cs_sid']);
			$stmt->bindParam(':cs_fid',		$_POST['cs_fid']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		header("Location: course_skill.php");
		die('Redirecting...');
		
	}
	elseif(!empty($_GET['cid']) && !empty($_GET['sklid']) && !empty($_GET['fid'])){
	
		$cid = $_GET['cid'];
		$sid = $_GET['sklid'];
		$fid = $_GET['fid'];
	}
	else
		die("No ID provided");
?>
<html>
	<head>
		<title>TADB - Course-Skill Edit</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
	<body>
		<?php include 'includes/header.php'; ?>
		<div class="container">
			<h2>Edit Course Skill</h2>
			<form name="edit" action="edit_course_skill.php" method="post">
				<input type="hidden" name="cs_cid" value="<?php echo $cid; ?>" />
				<input type="hidden" name="cs_sid" value="<?php echo $sid; ?>" />
				<input type="hidden" name="cs_fid" value="<?php echo $fid; ?>" />
				Course: <select name="cs_crs" class="form-control">
						<?php
							$res2 = $db->query("SELECT crs_id,crs_name FROM course ORDER BY crs_name");
							$res2->setFetchMode(PDO::FETCH_ASSOC);
							while($row = $res2->fetch())
							{
								$selopt = '';
								if($row['crs_id'] == $cid)
									$selopt = ' selected';
								echo "<option value=\"".$row['crs_id']."\"" . $selopt . ">".$row['crs_name']."</option>";
							}
						?>
					</select>
				Faculty: <select name="cs_fac" class="form-control">
						<?php
							$res3 = $db->query("SELECT fac_id,fac_fname,fac_lname FROM faculty ORDER BY fac_lname,fac_fname");
							$res3->setFetchMode(PDO::FETCH_ASSOC);
							while($row = $res3->fetch())
							{
								$selopt = '';
								if($row['fac_id'] == $fid)
									$selopt = ' selected';
								echo "<option value=\"".$row['fac_id']."\"".$selopt.">".$row['fac_fname']." ".$row['fac_lname']."</option>";
							}
						?>
					</select>
				Skill: <select name="cs_skl" class="form-control">
						<?php
							$res3 = $db->query("SELECT skl_id,skl_name FROM skill");
							$res3->setFetchMode(PDO::FETCH_ASSOC);
							while($row = $res3->fetch())
							{
								$selopt = '';
								if($row['skl_id'] == $sid)
									$selopt = ' selected';
								echo "<option value=\"".$row['skl_id']."\"".$selopt.">".$row['skl_name']."</option>";
							}
						?>
					</select>
				<input type="submit" class="btn btn-primary btn-lg" value="Edit" />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
		<?php $db = null; ?>
	</body>
</html>