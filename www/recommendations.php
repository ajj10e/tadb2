<?php
	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
		
	$skill_names = array();
	$skill_ids = array();
	$fac_ids = array();
	$fac_names = array();
	
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	$skill_count = 0;
	
	$cid = 1;
	$fid = 1;
	
	if(!empty($_GET['cfid'])) {
		$cfid = $_GET['cfid'];
		
		$cfid_array = explode(",",$cfid);
		$cid = $cfid_array[0];
		$fid = $cfid_array[1];
		
		try {
			$stmt = $db->prepare("SELECT crs_id, skl_id, fac_id, skl_name, skl_description
								  FROM course_skill
								  NATURAL JOIN skill
								  NATURAL JOIN course
								  WHERE crs_id=:cid
								  AND fac_id=:fid;");
			$stmt->bindParam(":cid", $cid);
			$stmt->bindParam(":fid", $fid);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			while($row = $stmt->fetch()){
				$skill_names[$skill_count] = $row['skl_name'];
				$skill_ids[$skill_count] = $row['skl_id'];
				$skill_count++;
			}
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
?>
<html>
	<head>
		<title>TADB - TA</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>

		<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css"/>
		<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDel(id)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+"?")
				if(opt==true)
					window.location.href = "delete.php?type=ta&id="+id;
			}
		</script>
		<div class="container">
			<h1>Recommendations</h1>
			<div class="table-responsive">
				<table id="myTable" class="table table-striped table-condensed" >
					<thead>
						<tr>
							<th>Name</th>
							<th>Recommendation</th>
							<?php
								foreach ($skill_names as $sn) {
									echo "<th>".$sn."</th>";
								}
							?>
							<th>Add</th>
						</tr>
					</thead>
					<tbody>
				<?php
					
					//Retrieve all TAs with at least one skill matching the current course skills
					try {
						$stmt = $db->prepare("SELECT ta_id,ta_fname,ta_lname,GROUP_CONCAT(skl_id) as skl_ids,GROUP_CONCAT(tskl_rating) as ta_ratings,SUM(tskl_rating) as tskl_sum 
											  FROM ta 
											  NATURAL JOIN ta_skill 
											  NATURAL JOIN skill 
											  NATURAL JOIN course_skill 
											  WHERE crs_id=:cid 
											  AND fac_id=:fid
											  GROUP BY ta_id 
											  ORDER BY tskl_sum;");
						$stmt->bindParam(":cid", $cid);
						$stmt->bindParam(":fid", $fid);
						$stmt->execute();
						$stmt->setFetchMode(PDO::FETCH_ASSOC);
					}
					catch(PDOException $e) {
						include_once('includes/error.php');
					}
					
					//Loop through result set and print out each TA and their skills
					while($row = $stmt->fetch())
					{
						echo "<tr>";
						
						//Print name
						echo "<td>";
						echo htmlspecialchars($row['ta_lname']) . ', ' . htmlspecialchars($row['ta_fname']);
						echo "</td>";
						
						//Break ratings and ids into arrays
						$ratings = explode(',', $row['ta_ratings']);
						$ids = explode(',', $row['skl_ids']);
						
						//Recommendation formula
						$sum = array_sum($ratings);
						
						//Print sum
						echo "<td>";
						echo $sum;
						echo "</td>";
						
						//Print skill ratings
						for($i = 0; $i < $skill_count; $i++) {
							echo "<td>";
							$n = 0;
							for($c = 0; $c < sizeof($ids); $c++) {
								if($skill_ids[$i] == $ids[$c]) {
									$n = $ratings[$c];
									$c = sizeof($ids);
								}
							}
							echo htmlspecialchars($n) . "</td>";
						}
						
						//Print edit and delete buttons
						echo "<td>";
						echo "<a class=\"btn btn-success btn-xs btn-block\" href=\"assignment.php?cid=".$cid."&taid=".$row['ta_id']."&fid=".$fid."#add"."\">Add</a>";
						echo "</td>";
						echo "</tr>";
					}
				?>

					</tbody>
				</table>
			</div>
			<hr />
			<form name="select_course" action="recommendations.php" method="get">
				Course: 
				<select class="form-control" name="cfid" onchange="this.form.submit()" >
				<?php
				
				//Populate the options in the course selection form
				//Add selected attribute to the current course, if there is one
				//TODO Add faculty
				try {
					/*
					$stmt = $db->prepare("SELECT DISTINCT co.crs_id, crs_num, crs_name, cs.fac_id, fac_fname, fac_lname
										  FROM course_skill as cs
										  RIGHT JOIN course as co
										  ON cs.crs_id=co.crs_id
										  LEFT JOIN faculty as fa
										  ON cs.fac_id=fa.fac_id
										  ORDER BY crs_num ASC;");
					*/
					$stmt = $db->prepare("select * 
										  from course_skill
										  natural join faculty
										  natural join course;");
					$stmt->execute();
					$stmt->setFetchMode(PDO::FETCH_ASSOC);
					while($row = $stmt->fetch()){
						
						$full_name = "Empty";
						if($row['fac_id']){

							$full_name = $row['fac_lname'].", ".$row['fac_fname'];
							
						}
						
						if(isset($cid) && $row['crs_id'] == $cid && $row['fac_id'] == $fid)
							echo "<option value=\"".$row['crs_id'].",".$row['fac_id']."\" selected >".$full_name.": ".$row['crs_num']." - ".$row['crs_name']."</option>";
						else
							echo "<option value=\"".$row['crs_id'].",".$row['fac_id']."\">".$full_name.": ".$row['crs_num']." - ".$row['crs_name']."</option>";
					}
				}
				catch(PDOException $e) {
					include_once('includes/error.php');
				}
				?>
				</select>
				<br />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
	</body>
	<script>
		$(document).ready(function(){
			$('#myTable').dataTable({
				
			});
		});
	</script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>
</html>