<?php

	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	
	$sid = NULL;
	$snum = $sname = $sdesc = '';
	
	//If post parameters are not empty, update the record.
	//If one or more are empty, but get parameters are not, retrieve all area names and store in res.
	//Otherwise, die.
	if(!empty($_POST['s_id']))
	{
	
		try{
			$stmt = $db->prepare('UPDATE skill 
								  SET skl_name=:s_name,skl_description=:s_desc 
								  WHERE skl_id=:s_id');
			$stmt->bindParam(':s_name', $_POST['s_name']);
			$stmt->bindParam(':s_desc', $_POST['s_desc']);
			$stmt->bindParam(':s_id', 	$_POST['s_id']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		header("Location: skill.php");
		die('Redirecting...');
	}
	elseif(!empty($_GET['id'])){
	
		$sid = $_GET['id'];
		try{
			$stmt = $db->prepare('SELECT skl_name,skl_description 
									FROM skill 
									WHERE skl_id=:sid');
			$stmt->bindParam(':sid', $sid);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$res = $stmt->fetch();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		$sname = isset($res['skl_name']) ? $res['skl_name'] : '';
		$sdesc = isset($res['skl_description']) ? $res['skl_description'] : '';
	}
	else
		die("No ID provided");
?>
<html>
	<head>
		<title>TADB - Skill Edit</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
	<body>
		<?php include 'includes/header.php'; ?>
		<div class="container">
			<h2>Edit Skill</h2>
			<form name="edit_skill" action="edit_skill.php" method="post">
				<input type="hidden" name="s_id" value="<?php echo $sid; ?>" />
				Skill Name: <input type="text" class="form-control" name="s_name" value="<?php echo $sname; ?>" />
				Description: <input type="text" class="form-control" name="s_desc" value="<?php echo $sdesc; ?>" />
				<input type="submit" class="btn btn-primary btn-lg" value="Edit" />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
		<?php $db = null; ?>
	</body>
</html>