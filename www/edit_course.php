<?php

	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
	
	$cid = NULL;
	$cname = $cdesc = $cnum = '';
	
	//If post parameters are not empty, update the record.
	//If one or more are empty, but get parameters are not, retrieve all area names and store in res.
	//Otherwise, die.
	if(!empty($_POST['crs_id']))
	{
		//what should we actually do to check for this whole mess?

		try {
			$stmt = $db->prepare('UPDATE course 
								  SET crs_num=:crs_num, crs_name=:crs_name, crs_description=:crs_desc 
								  WHERE crs_id=:crs_id');
			$stmt->bindParam(':crs_num',	$_POST['crs_num']);
			$stmt->bindParam(':crs_name',	$_POST['crs_name']);
			$stmt->bindParam(':crs_desc',	$_POST['crs_desc']);
			$stmt->bindParam(':crs_id',	$_POST['crs_id']);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		header("Location: course.php");
		die('Redirecting...');
	}
	elseif(!empty($_GET['id'])){
	
		$cid = $_GET['id'];
		
		try{
			$stmt = $db->prepare('SELECT crs_id,crs_num,crs_name,crs_description
								  FROM course
								  WHERE crs_id=:cid');
			$stmt->bindParam(':cid', $cid);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$res = $stmt->fetch();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}	
		$cname = isset($res['crs_name']) ? $res['crs_name'] : '';
		$cdesc = isset($res['crs_description']) ? $res['crs_description'] : '';
		$cnum = isset($res['crs_num']) ? $res['crs_num'] : '';
	}
	else
		die("No ID provided");
?>
<html>
	<head>
		<title>TADB - Course Edit</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
	<body>
		<?php include 'includes/header.php'; ?>
		<div class="container">
			<h2>Edit Course</h2>
			<form name="edit_skill" action="edit_course.php" method="post">
				<input type="hidden" name="crs_id" value="<?php echo $cid; ?>" />
				Course Name: <input type="text" class="form-control" name="crs_name" value="<?php echo $cname; ?>" />
				Course Number: <input type="text" class="form-control" name="crs_num" value="<?php echo $cnum; ?>" />
				Description: <input type="text" class="form-control" name="crs_desc" value="<?php echo $cdesc; ?>" />
				<input type="submit" class="btn btn-primary btn-lg" value="Edit" />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
		<?php $db = null; ?>
	</body>
</html>