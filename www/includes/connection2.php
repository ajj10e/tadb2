<?php
$IP="local";
//$IP="remote";
include './includes/config.php';
if ($IP=="local")
{
  $dsn = 'mysql:host=localhost;port=3306;dbname=ajh11m_dis;mysql_local_infile=1';
  $username = 'ajh11m';
  $password = 'dnvtg7cw';
}
else
{
  $dsn = 'mysql:host=yourremotehost;port=3306;dbname=yourremotedatabase;mysql_local_infile=1';
  $username = 'yourremoteusername';
  $password = 'yourremotepassword';
}

try 
{
//instantiate new PDO connection
  $db = new PDO($dsn, $username, $password);

  //Three error modes: PDO::ERRMODE_SILENT, PDO::ERRMODE_WARNING, PDO::ERRMODE_EXCEPTION

  //PDO::ERRMODE_EXCEPTION: mode for most situations. 
  //Fires an exception, allowing errors to be handled gracefully and hide data that might be exploited 
  //$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

  //echo "Connected successfully using pdo extension!<br /><br />";
} 
catch (PDOException $e) 
{
  //echo $e->getMessage();
  $error = $e->getMessage();
  
  //in production environment
  //$error = "Connection failed. Please see system administrator";
  include('error.php');
  exit();
}
?>