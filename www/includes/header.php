<?php
	require_once('includes/config.php');
	$parent = basename($_SERVER["SCRIPT_FILENAME"]);
?>
<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php"><img src="images/fsu-logo.png" style="width:auto;height:40" /></a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<?php 
				if(isset($_SESSION['rank'])){
					$rank = $_SESSION['rank'];
					if($rank==1){
						echo '<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Menu <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="assignment.php">Assignments</a></li>
								<li><a href="recommendations.php">Recommendations</a></li>
								<li class="divider"></li>
								<li><a href="area.php">Areas</a></li>
								<li><a href="course.php">Courses</a></li>
								<li><a href="course_skill.php">Course Skills</a></li>
								<li><a href="faculty.php">Faculty</a></li>
								<li><a href="skill.php">Skills</a></li>
								<li><a href="ta.php">TA</a></li>
								<li class="divider"></li>
								<li><a href="ta_area.php">TA Areas</a></li>
								<li><a href="ta_skill.php">TA Skills</a></li>
								<li class="divider"></li>
								<li><a href="admin.php">Admin</a></li>
							</ul>
							</li>';
					}
					else{
						echo '<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Menu <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="ta_profile.php">My Profile</a></li>
							</ul>
							</li>';
					}
				}
				elseif($parent != "index.php"){
					die('Access denied');
				}
				?>
			</ul>
			<?php if(isset($_SESSION['rank']))
			echo '<ul class="nav navbar-nav navbar-right">
				<li><a class="btn btn-danger navbar-btn" href="logout.php" style="color:#ffffff">Logout</a></li>
			</ul>';
			?>
		</div>
	</div>
</nav>