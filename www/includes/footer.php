<div class="navbar navbar-default">
	<div class="container">
		<div class="navbar-left">
			Project Team Members: <a href="http://www.austinhickey.com/" target="_blank">Austin Hickey</a>, <a href="mailto:josephpatrickgiurintano@gmail.com">Joseph Patrick Giurintano</a>, and <a href="mailto:adam.j.jankowski@gmail.com">Adam Janikowski</a>. All rights reserved.<br/>
			Intended for use within Florida State University only.
		</div>
		<div class="navbar-right">
			This page was last modified on: <br/>
			<?php echo date("n/j/Y G:i:s",filemtime($_SERVER["SCRIPT_FILENAME"])); ?>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.js"></script>
