<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	if(session_status() == PHP_SESSION_NONE)
		session_start();

	//in order of least to greatest (zero-based)
	global $ranks;
	$ranks = array('ta' => 0,'admin' => 1);
	
?>