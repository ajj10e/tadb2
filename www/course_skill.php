<?php

	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
?>
<html>
	<head>
		<title>TADB - Course Skills</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
		<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css"/> <link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDel(id,id2,id3)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+":"+id2+":"+id3+"?")
				if(opt==true)
					window.location.href = "delete.php?type=course_skill&id="+id+"&id2="+id2+"&id3="+id3;
			}
		</script>
		<div class="container">
			<h1>Course Skills</h1>
			<div class="table-responsive">
			<table id="myTable" class="table" >
				<thead>
					<tr>
						<th>Course Name</th>
						<th>Faculty Name</th>
						<th>Skill Name</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<?php
				try{
					$statement = $db->prepare("SELECT course_skill.crs_id,crs_name,crs_num,course_skill.fac_id,fac_fname,fac_lname,course_skill.skl_id,skl_name FROM course_skill NATURAL JOIN course NATURAL JOIN skill NATURAL JOIN faculty ORDER BY crs_name,fac_lname,fac_fname");
				/*	$statement->bindParam(":crsSkill_crsId", "course_skill.crs_id");
					$statement->bindParam(":crsName", "crs_name");
					$statement->bindParam(":crsSkill_facId", "course_skill.fac_id");
					$statement->bindParam(":facFname", "fac_fname");
					$statement->bindParam(":facLname", "fac_lname");
					$statement->bindParam(":crsSkill_sklId", "course_skill.skl_id");
					$statement->bindParam(":crsSkill", "course_skill");
					$statement->bindParam(":course", "course");
					$statement->bindParam(":skill", "skill");
					$statement->bindParam(":faculty", "faculty");
				*/
					$statement->execute();
					//$res = $mysqlconn->query("SELECT course_skill.crs_id,crs_name,course_skill.fac_id,fac_fname,fac_lname,course_skill.skl_id,skl_name FROM course_skill NATURAL JOIN course NATURAL JOIN skill NATURAL JOIN faculty ORDER BY crs_name,fac_lname,fac_fname");
					$statement->setFetchMode(PDO::FETCH_ASSOC);
				}catch(PDOException $e){
					$e->getMessage();
					sleep(10);
					die();
				}
					while($row = $statement->fetch())
					{
						echo "<tr>";
						echo "<td>";
							echo htmlspecialchars($row['crs_name']);
						echo "</td>";
						echo "<td>";
							echo htmlspecialchars($row['fac_lname']) . ', ' . htmlspecialchars($row['fac_fname']);
						echo "</td>";
						echo "<td>";
							echo ucfirst(htmlspecialchars($row['skl_name']));
						echo "</td>";
						echo "<td>";
							echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_course_skill.php?cid=".$row['crs_id']."&sklid=".$row['skl_id']."&fid=".$row['fac_id']."\">Edit</a>";
						echo "</td>";
						echo "<td>";
							echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDel(".$row['crs_id'].",".$row['skl_id'].",".$row['fac_id'].")\">Delete</button>";
						echo "</td>";
						echo "</tr>";
					}
				?>
			</table>
			</div>
			<hr />
			<h2>Add Course Skill</h2>
			<form name="add_crsskill" action="add.php" method="post">
				<input type="hidden" name="type" value="course_skill" />
				Course: <select class="form-control" name="c_id">
								<?php
									$statement2 = $db->prepare("SELECT crs_id,crs_name,crs_num FROM course ORDER BY crs_name");
							/*		$statement2->bindParam(":course", "course");
									$statement2->bindParam(":crsId", "crs_id");
									$statement2->bindParam(":crsName", "crs_name");
							*/
									$statement2->execute();
									//$res2 = $mysqlconn->query("SELECT crs_id,crs_name FROM course ORDER BY crs_name");
									while($row = $statement2->fetch(PDO::FETCH_ASSOC))
										echo "<option value=\"".$row['crs_id']."\">".$row['crs_num']." - ".$row['crs_name']."</option>";
								?>
							</select>
				Faculty: <select class="form-control" name="f_id">
								<?php
									$statement3 = $db->prepare("SELECT fac_id,fac_fname,fac_lname FROM faculty ORDER BY fac_lname,fac_fname");
								/*	$statement3->bindParam(":facId", "fac_id");
									$statement3->bindParam(":facFname", "fac_fname");
									$statement3->bindParam(":facLname", "fac_lname");
									$statement3->bindParam(":faculty", "faculty");
								*/
									$statement3->execute();
									//$res3 = $mysqlconn->query("SELECT fac_id,fac_fname,fac_lname FROM faculty ORDER BY fac_lname,fac_fname");
									while($row = $statement3->fetch(PDO::FETCH_ASSOC))
										echo "<option value=\"".$row['fac_id']."\">".$row['fac_lname'].", ".$row['fac_fname']."</option>";
								?>
							</select>
				Skill: <select class="form-control" name="s_id">
								<?php
									$statement4 = $db->prepare("SELECT skl_id,skl_name FROM skill");
								/*
									$statement4->bindParam(":sklId", "skl_id");
									$statement4->bindParam(":sklName", "skl_name");
									$statement4->bindParam(":skill", "skill");
								*/
									$statement4->execute();
									//$res3 = $mysqlconn->query("SELECT skl_id,skl_name FROM skill");
									while($row = $statement4->fetch(PDO::FETCH_ASSOC))
										echo "<option value=\"".$row['skl_id']."\">".$row['skl_name']."</option>";
								?>
							</select>
				<input type="submit" class="btn btn-primary btn-lg" value="Add" />
			</form>
		</div>
		<?php 
		include 'includes/footer.php'; 
		$db = null;
		?>
	</body>
	<script>
		$(document).ready(function(){
			$('#myTable').dataTable({
				"columns": [
					null,
					null,
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
		});
	</script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>
</html>