<?php

	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
		
	$table = $_GET['type'];
	//$id, $id2, $id3;
	
	$id = isset($_GET['id']) ? $_GET['id'] : '';

	$id2 = isset($_GET['id2']) ? $_GET['id2'] : '';

	$id3 = isset($_GET['id3']) ? $_GET['id3'] : '';
	
	$rank = $_SESSION['rank'];
	$rankneeded = $ranks['admin'];

	if($table == "course")
	{
		try {
			$stmt = $db->prepare("DELETE FROM course WHERE crs_id=:id");
			$stmt->bindParam(":id", $id);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif ($table == "user"){
		try {
			$stmt = $db->prepare("DELETE FROM user WHERE usr_id=:id");
			$stmt->bindParam(":id", $id);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		$table = "admin";
	}
	elseif($table == "faculty")
	{
		$id_col = "fac_id";
		try {
			$stmt = $db->prepare("DELETE FROM faculty WHERE fac_id=:id");
			$stmt->bindParam(":id", $id);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "area")
	{
		$id_col = "area_id";
		try {
			$stmt = $db->prepare("DELETE FROM area WHERE area_id=:id");
			$stmt->bindParam(":id", $id);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "ta")
	{
		$id_col = "ta_id";
		try {
			$stmt = $db->prepare("DELETE FROM ta WHERE ta_id=:id");
			$stmt->bindParam(":id", $id);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "skill")
	{
		$id_col = "skl_id";
		try {
			$stmt = $db->prepare("DELETE FROM skill WHERE skl_id=:id");
			$stmt->bindParam(":id", $id);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "ta_skill")
	{
		
		try {
			$stmt = $db->prepare("DELETE FROM ta_skill WHERE ta_id=:id AND skl_id=:id2");
			$stmt->bindParam(":id", $id);
			$stmt->bindParam(":id2", $id2);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "course_skill")
	{
		
		try {
			$stmt = $db->prepare("DELETE FROM course_skill WHERE crs_id=:id AND skl_id=:id2 AND fac_id=:id3");
			$stmt->bindParam(":id", $id);
			$stmt->bindParam(":id2", $id2);
			$stmt->bindParam(":id3", $id3);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "ta_area")
	{
		
		try {
			$stmt = $db->prepare("DELETE FROM ta_area WHERE ta_id=:id AND area_id=:id2");
			$stmt->bindParam(":id", $id);
			$stmt->bindParam(":id2", $id2);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "assignment")
	{
	
		try {
			$stmt = $db->prepare("DELETE FROM assignment WHERE ta_id=:id AND fac_id=:id2 AND crs_id=:id3");
			$stmt->bindParam(":id", $id);
			$stmt->bindParam(":id2", $id2);
			$stmt->bindParam(":id3", $id3);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "tapa")
	{
	
		try {
			$stmt = $db->prepare("DELETE FROM ta_area WHERE ta_id=:id AND area_id=:id2");
			$stmt->bindParam(":id",$id);
			$stmt->bindParam(":id2",$id2);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	elseif($table == "taps")
	{	
		
		try {
			$stmt = $db->prepare("DELETE FROM ta_skill WHERE ta_id=:id and skl_id=:id2");
			$stmt->bindParam(":id",$id);
			$stmt->bindParam(":id2",$id2);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	else
		die("Bad type");
	
	header('Refresh: 0;URL='.$table.'.php');
	echo "Successfully deleted record";
	echo "<br/>Please wait while you are redirected back...";
	
	/*
	if($query)
	{
		header('Refresh: 0;URL='.$table.'.php');
		echo "Successfully deleted record";
		echo "<br/>Please wait while you are redirected back...";
	}else
		die("How did you screw up a delete?! Dying...<br/>".$mysqlconn->error);
	*/

?>