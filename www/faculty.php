<?php
	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
?>
<html>
	<head>
		<title>TADB - Faculty</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
		<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css"/> <link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDel(id)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+"?")
				if(opt==true)
					window.location.href = "delete.php?type=faculty&id="+id;
			}
		</script>
		<div class="container">
			<h1>Faculty</h1>
			<div class="table-responsive">
			<table id="myTable" class="table" >
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<?php
				try{
					$stat1 = $db->prepare("SELECT fac_id,fac_fname,fac_lname,fac_email FROM faculty ORDER BY fac_lname,fac_fname");
					$stat1->execute();
					$stat1->setFetchMode(PDO::FETCH_ASSOC);
				}catch(PDOException $e){
					$e->getMessage();
					sleep(10);
					die();
				}
					//$res = $mysqlconn->query("SELECT fac_id,fac_fname,fac_lname,fac_email FROM faculty ORDER BY fac_lname,fac_fname");
					while($row = $stat1->fetch())
					{
						echo "<tr>";
						echo "<td>";
							echo htmlspecialchars($row['fac_lname']) . ', ' . htmlspecialchars($row['fac_fname']);
						echo "</td>";
						echo "<td>";
							echo htmlspecialchars($row['fac_email']);
						echo "</td>";
						echo "<td>";
							echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_faculty.php?id=".$row['fac_id']."\">Edit</a>";
						echo "</td>";
						echo "<td>";
							echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDel(".$row['fac_id'].")\">Delete</button>";
						echo "</td>";
						echo "</tr>";
					}
				?>
			</table>
			</div>
			<hr />
			<h2>Add Faculty</h2>
			<form name="add_faculty" action="add.php" method="post">
				<input type="hidden" name="type" value="faculty" />
				First name: <input type="text" class="form-control" name="f_fname" />
				Last name: <input type="text" class="form-control" name="f_lname" />
				E-mail: <input type="email" class="form-control" name="f_email" />
				<input type="submit" class="btn btn-primary btn-lg" value="Add" />
			</form>
		</div>
		<?php include 'includes/footer.php'; ?>
	</body>
	<script>
		$(document).ready(function(){
			$('#myTable').dataTable({
				"columns": [
					null,
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
		});
	</script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>
</html>