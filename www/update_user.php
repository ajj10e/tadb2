<?php

include_once('includes/connection.php');
$cfg = include_once('includes/config.php');
if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
	die('Access denied.');

$uid = $_GET['id'];
$attr = $_GET['attr'];
$val = $_GET['val'];

if (empty($attr) || empty($val)) {
	die("Toggle failed. Empty parameters.");
}

if($attr == "status") {
	
	try {
		$stmt = $db->prepare("update user 
							  set usr_status=:status
							  where usr_id=:uid");
		$status = "Pending";
		if ($val == "Pending") {
			$status = "Approved";
		}
		
		$stmt->bindParam(":status", $status);
		$stmt->bindParam(":uid", $uid);
		$stmt->execute();
	}
	catch(PDOException $e) {
		include_once('includes/error.php');
	}
}
else if ($attr == "role") {

	try {
		$stmt = $db->prepare("update user 
							  set usr_role=:role
							  where usr_id=:uid");
		$role = "ta";
		if ($val == "ta") {
			$role = "admin";
		}
		$stmt->bindParam(":role", $role);
		$stmt->bindParam(":uid", $uid);
		$stmt->execute();
	}
	catch(PDOException $e) {
		include_once('includes/error.php');
	}
}
else {
	die("Toggle failed. Invalid attr parameter.");
}

header('Refresh: 0;URL=admin.php');
echo "Toggle was successful.";
echo "<br/>Please wait while you are redirected...";

?>