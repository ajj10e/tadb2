<?php
	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
?>
<html>
	<head>
		<title>TADB - Skills</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
		<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css"/> <link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDel(id)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+"?")
				if(opt==true)
					window.location.href = "delete.php?type=skill&id="+id;
			}
		</script>
		<div class="container">
			<h1>Skills</h1>
			<div class="table-responsive">
			<table id="myTable" class="table" >
				<thead>
					<tr>
						<th>Name</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<?php
				try {
					$statement = $db->prepare("SELECT skl_id,skl_name,skl_description FROM skill ORDER BY skl_name ASC");
				/*	$statement->bindParam(":sklId", "skl_id");
					$statement->bindParam(":sklName", "skl_name");
					$statement->bindParam(":sklDescription", "skl_description");
				*/	$statement->execute();
					$statement->setFetchMode(PDO::FETCH_ASSOC);
				}catch (PDOException $e){
					$e->getMessage();
					sleep(10);
					die();
				}
					//$res = $mysqlconn->query("SELECT skl_id,skl_name,skl_description FROM skill ORDER BY skl_name ASC");
					while($row = $statement->fetch())
					{
						echo "<tr>";
						echo "<td>";
							echo ucfirst(htmlspecialchars($row['skl_name']));
						echo "</td>";
						echo "<td>";
							echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_skill.php?id=".$row['skl_id']."\">Edit</a>";
						echo "</td>";
						echo "<td>";
							echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDel(".$row['skl_id'].")\">Delete</button>";
						echo "</td>";
						echo "</tr>";
					}
				?>
			</table>
			</div>
			<hr />
			<h2>Add Skill</h2>
			<form name="add_skill" action="add.php" method="post">
				<input type="hidden" name="type" value="skill" />
				Skill Name: <input type="text" class="form-control" name="s_name" />
				Description: <input type="text" class="form-control" name="s_desc" />
				<input type="submit" class="btn btn-primary btn-lg" value="Add" />
			</form>
		</div>
		<?php 
		include 'includes/footer.php';
		$db = null;
		?>
	</body>
	<script>
		$(document).ready(function(){
			$('#myTable').dataTable({
				"columns": [
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
		});
	</script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>
</html>