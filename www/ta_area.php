<?php
	include_once('includes/connection.php');
	$cfg = include_once('includes/config.php');
	if(!isset($_SESSION['rank']) || $_SESSION['rank'] < $ranks['admin'])
		die('Access denied.');
?>
<html>
	<head>
		<title>TADB - TA Areas</title>
		<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
		<link rel="stylesheet" type='text/css' href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css">
		<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css"/> <link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		<script>
			function confirmDel(id,id2)
			{
				var opt = confirm("Are you sure you want to delete id: "+id+"?")
				if(opt==true)
					window.location.href = "delete.php?type=area&id="+id+"&id2="+id2;
			}
		</script>
		<div class="container">
			<h1>TA Areas</h1>
			<div class="table-responsive">
			<table id="myTable" class="table" >
				<thead>
					<tr>
						<th>TA Name</th>
						<th>Area Name</th>
						<th>FTE</th>
						<th>Semester</th>
						<th>Year</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<?php

				try{
					$stat1 = $db->prepare("SELECT ta_id,area_id,CONCAT(ta_lname,', ',ta_fname) as ta_name,area_name,area_fte,area_semester,area_year FROM ta_area NATURAL JOIN ta NATURAL JOIN area ORDER BY ta_lname,ta_fname,area_name");
				/*	$stat1->bindParam(":taId", "ta_id");
					$stat1->bindParam(":areaId", "area_id");
					$stat1->bindParam(":taFname", "ta_fname");
					$stat1->bindParam(":space", " ");
					$stat1->bindParam(":taLname", "ta_lname");
					$stat1->bindParam(":taName", "ta_name");
					$stat1->bindParam(":areaSemester", "area_semester");
					$stat1->bindParam(":areaYear", "area_year");
					$stat1->bindParam(":taArea", "ta_area");
					$stat1->bindParam(":ta", "ta");
					$stat1->bindParam(":area", "area");
				*/	$stat1->execute();
					//$res = $mysqlconn->query("SELECT ta_id,area_id,CONCAT(ta_fname, ' ',ta_lname) as ta_name,area_name,area_fte,area_semester,area_year FROM ta_area NATURAL JOIN ta NATURAL JOIN area ORDER BY ta_lname,ta_fname,area_name");
					$stat1->setFetchMode(PDO::FETCH_ASSOC);
				}catch (PDOException $e){
					$e->getMessage();
					sleep(10);
					die();
				}
					while($row = $stat1->fetch())

					{
						echo "<tr>";
						echo "<td>";
							echo htmlspecialchars($row['ta_name']);
						echo "</td>";
						echo "<td>";
							echo htmlspecialchars($row['area_name']);
						echo "</td>";
						echo "<td>";
							echo htmlspecialchars($row['area_fte']);
						echo "</td>";
						echo "<td>";
							$semester = htmlspecialchars($row['area_semester']);
							switch ($semester){
								case "sum":
									echo "Summer";
									break;
								case "fall":
									echo "Fall";
									break;
								case "spr":
									echo "Spring";
									break;
							}
						echo "</td>";
						echo "<td>";
							echo htmlspecialchars($row['area_year']);
						echo "</td>";
						echo "<td>";
							echo "<a class=\"btn btn-warning btn-xs btn-block\" href=\"edit_ta_area.php?taid=".$row['ta_id']."&aid=".$row['area_id']."\">Edit</a>";
						echo "</td>";
						echo "<td>";
							echo "<button class=\"btn btn-danger btn-xs btn-block\" onclick=\"confirmDel(".$row['ta_id'].",".$row['area_id'].")\">Delete</button>";
						echo "</td>";
						echo "</tr>";
					}
				?>
			</table>
			</div>
			<hr />
			<h2>Add TA Area</h2>
			<form name="add_area" action="add.php" method="post">
				<input type="hidden" name="type" value="ta_area" />
				TA: <select class="form-control" name="taa_ta">
						<?php
							$stat2 = $db->prepare("SELECT ta_id,CONCAT(ta_lname,', ',ta_fname) as ta_name FROM ta ORDER BY ta_lname,ta_fname");
						/*	$stat2->bindParam(":taId", "ta_id");
							$stat2->bindParam(":taFname", "ta_fname");
							$stat2->bindParam(":space", " ");
							$stat2->bindParam(":taLname", "ta_lname");
							$stat2->bindParam(":taName", "ta_name");
							$stat2->bindParam(":ta", "ta");
						*/	$stat2->execute();
							//$res2 = $mysqlconn->query("SELECT ta_id,CONCAT(ta_fname,' ',ta_lname) as ta_name FROM ta ORDER BY ta_lname,ta_fname");
							while($row = $stat2->fetch(PDO::FETCH_ASSOC))
								echo "<option value=\"".$row['ta_id']."\">".$row['ta_name']."</option>";
						?>
					</select>
				Area: <select class="form-control" name="taa_aid">
						<?php
							$stat3 = $db->prepare("SELECT area_id,area_name FROM area ORDER BY area_name");
						/*	$stat3->bindParam(":areaId", "area_id");
							$stat3->bindParam(":areaName", "area_name");
							$stat3->bindParam(":area", "area");
						*/	$stat3->execute();
							//$res3 = $mysqlconn->query("SELECT area_id,area_name FROM area ORDER BY area_name");
							while ($row = $stat3->fetch(PDO::FETCH_ASSOC))
								echo "<option value=\"".$row['area_id']."\">".$row['area_name']."</option>";
						?>
					</select>
				Area FTE: <input type="number" class="form-control" name="a_fte" min="0" max="9.99" step="0.01" value="1.00" />
				Area Semester: <select class="form-control" name="a_sem">
					<option value="fall">Fall</option>
					<option value="spr">Spring</option>
					<option value="sum">Summer</option>
				</select>
				Area Year: 
				<select class="form-control" name="a_year">
					<option value="2000">2000</option>
					<option value="2001">2001</option>
					<option value="2002">2002</option>
					<option value="2003">2003</option>
					<option value="2004">2004</option>
					<option value="2005">2005</option>
					<option value="2006">2006</option>
					<option value="2007">2007</option>
					<option value="2008">2008</option>
					<option value="2009">2009</option>
					<option value="2010">2010</option>
					<option value="2011">2011</option>
					<option value="2012">2012</option>
					<option value="2013">2013</option>
					<option value="2014">2014</option>
					<option value="2015">2015</option>
					<option value="2016">2016</option>
					<option value="2017">2017</option>
					<option value="2018">2018</option>
					<option value="2019">2019</option>
					<option value="2020">2020</option>
				</select>
				<input type="submit" class="btn btn-primary btn-lg" value="Add" />
			</form>
		</div>
		<?php 
		include 'includes/footer.php';
		$db = null;
		?>
	</body>
	<script>
		$(document).ready(function(){
			$('#myTable').dataTable({
				"columns": [
					null,
					null,
					null,
					null,
					null,
					{"orderable":false},
					{"orderable":false}
				]
			});
		});
	</script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>
</html>