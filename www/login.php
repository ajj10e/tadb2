<?php
	include_once 'includes/connection.php';
	$uid = $email = $pass = $rank = 0;
	
	if (!empty($_GET['r'])) {
		
		$pass = $_POST['pass'];
		$salt = uniqid(mt_rand(), true);
		$hash = hash('sha512', $salt.$pass);
		$confirm = $_POST['confirm'];
		$link = $_GET['r'];
		$uid = 0;
		
		if ($pass != $confirm) {
			die('Passwords don\'t match.');
		}
		
		try {
			
			$stmt = $db->prepare('select usr_id,rst_status
								  from reset
								  where rst_link=:link
								  order by rst_id desc
								  limit 1;');
			$stmt->bindParam(':link', $link);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			
			if($row = $stmt->fetch()) {
			
				if($row['rst_status'] == 'pending') {
					die('Your password reset request is pending. 
						 Please contact the administrator for assistance.');
				}
				else if($row['rst_status'] == 'denied') {
					die('Your password reset request has been denied. 
						 Please contact the administrator for assistance.');
				}
				else {
					$uid = $row['usr_id'];
				}
			}
			else {
				die('Link not found. Please contact the administrator for assistance.');
			}
			
			$stmt = $db->prepare('update user 
								  set usr_pass=:hash, usr_salt=:salt
								  where usr_id=:uid;');
			$stmt->bindParam(':hash', $hash);
			$stmt->bindParam(':salt', $salt);
			$stmt->bindParam(':uid', $uid);
			$stmt->execute();
			
			header('Location: index.php');
			die('Success');
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
	}
	else if (!empty($_POST['email']) && !empty($_POST['pass']) && !empty($_POST['fname']) && !empty($_POST['lname'])) {
		
		$email = $_POST['email'];
		$pass = $_POST['pass'];
		$fname = $_POST['fname'];
		$lname = $_POST['lname'];
		$salt = uniqid(mt_rand(), true);
		$pass = $_POST['pass'];
		$hash = hash('sha512', $salt.$pass);
		
		try{
			$stmt = $db->prepare('insert into user
								  (usr_fname,usr_lname,usr_email,usr_salt,usr_pass)
								  values
								  (:fname,:lname,:email,:salt,:pass)');
			$stmt->bindParam(':fname', $fname);
			$stmt->bindParam(':lname', $lname);
			$stmt->bindParam(':email', $email);
			$stmt->bindParam(':pass', $hash);
			$stmt->bindParam(':salt', $salt);
			$stmt->execute();
			$_SESSION['email'] = $email;
			$_SESSION['rank'] = 0;
			header('Location: index.php');
			die('Success');
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	else if(!empty($_POST['email']) && !empty($_POST['pass'])) {
	
		$email = $_POST['email'];
		$pass = $_POST['pass'];
		
		try {
			$stmt = $db->prepare("SELECT usr_salt,usr_pass,usr_role,usr_status
								  FROM user
								  WHERE usr_email=:email");
			$stmt->bindParam(":email", $email);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			if($row = $stmt->fetch()) {
				
				if($row['usr_status'] == "Pending")
					die("Your account status is currently pending. Please contact your administrator for assistance");
				
				if(hash('sha512', $row['usr_salt'].$pass) == $row['usr_pass']) {
					
					$rank = 0;
					$role = $row['usr_role'];
					
					if ($role == 'Admin') {
						$rank = 1;
					}
					$_SESSION['email'] = $email;
					$_SESSION['rank'] = $rank;
					header('Location: index.php');
					
				}
				else {
					die('Incorrect password!');
				}
			}
			else {
				die('Incorrect email!');
			}
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
	}
	else if ($_POST['email']) {
		
		$email = $_POST['email'];
		
		try {
			$lname = "";
			$salt = uniqid(mt_rand(), true);
			$stmt = $db->prepare("SELECT usr_id, usr_lname
								  FROM user
								  WHERE usr_email=:email");
			$stmt->bindParam(":email", $email);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			
			if($row = $stmt->fetch()) {
				$uid = $row['usr_id'];
				$lname = $row['usr_lname'];
			}
			else {
				die('Email not found.');
			}
			
			$link = hash('sha512', $uid.$lname.$salt);
			$stmt = $db->prepare("insert into reset 
								  (usr_id,rst_link,rst_status)
								  values
								  (:uid,:link)");
			$stmt->bindParam(":uid", $uid);
			$stmt->bindParam(":link", $link);
			$stmt->execute();
		}
		catch(PDOException $e) {
			include_once('includes/error.php');
		}
		
		$email_from = "no-reply@tadb.qcitr.com";
		$subject = "TADB Password Reset";
		$message = "Please click the link below to reset your password:\nhttp://tadb.local/www/index.php?r=$link";
		$headers = 'From: '.$email_from."\r\n".'Reply-To: '.$email_from."\r\n".'X-Mailer: PHP/' . phpversion();
		
		mail($email,$subject,$message,$headers);
		
		die('Password reset request sent successfully. Please check your email for your reset link.');
	}
	else{
		die('Invalid email or password');
	}
?>