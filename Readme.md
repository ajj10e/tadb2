# TADB #

An application to match TA's to courses based of a self-rated set of skills and skills required for courses.

## Information ##
URL: https://ispace-2014.cci.fsu.edu/~ajh11m/dis/
### Login ###
* Admin = admin@my.fsu.edu:admin
* TA = ta@my.fsu.edu:ta

ERD is available on the repo

All commits automatically deployed to iSpace. The icon below will tell you the status of the revisions deployment:

[![Deployment status from dploy.io](https://lis4905spr15.dploy.io/badge/88313865895984/19154.png)](http://dploy.io)

# *TADB Migration:* #

### *Caution!* ###
    * Since we *all* have access to modify files and data--*please* be careful. 
    * When in doubt, *always* make an additional backup, and include dates/timestamps in backup names.
    * As previously mentioned, please follow these guidelines:
        * https://guides.github.com/introduction/flow/index.html
        * http://scottchacon.com/2011/08/31/github-flow.html

## Comments: ##
1. Please change the *footer*:

- *No* copyright: the idea, requirements, project design, and testing have been ongoing, prior to your coming onboard.

(Much the same as you wouldn't appreciate a copyright on something that you've already worked on.)

- In lieu of a copyright, feel free to include "Project Team Members:"...

- Also, make sure that the footer is responsive (i.e., works with various devices--currently, it does not). Easy fix.

- We all should have FTP and DB access using the following credentials (please test accessibility as soon as possible):

## Web Hosting ##
    * 128.186.72.64
    * LDAP login
    * Upon login, place files and sub-directories in the following sub-directory (requires manual path directive, do *not* place in "html" directory):
    * var/www/tadb/www

## DB Hosting ##
    * 128.186.72.67
    * db: tadb
    * user: tadb
    * access: 1Tadb1

## Testing ##
    * Code-Check: CRUD and data validation functionality 
    * Test with both valid and invalid data
    * Test SQL injection

### Issues ###
    * Please post any questions/issues under "issues"