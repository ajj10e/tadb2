-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.5.40-0ubuntu0.14.04.1-log - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4882
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for ajh11m_dis
CREATE DATABASE IF NOT EXISTS `ajh11m_dis` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ajh11m_dis`;


-- Dumping structure for table ajh11m_dis.admin
DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `adm_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `adm_email` varchar(100) NOT NULL,
  `adm_salt` varchar(64) NOT NULL,
  `adm_pass` char(128) NOT NULL,
  PRIMARY KEY (`adm_id`),
  UNIQUE KEY `adm_name_UNIQUE` (`adm_email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ajh11m_dis.admin: ~2 rows (approximately)
DELETE FROM `admin`;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`adm_id`, `adm_email`, `adm_salt`, `adm_pass`) VALUES
	(1, 'ajh11m@my.fsu.edu', 'austin', 'dfddfa6bd5e570ed0d4a272a4a115efff542f9e461157e26e4246e7a14387817eba6f167782d31241f6ecbe3adf77601b3af5d4eb080d2bb79ac2e810e6d0056'),
	(2, 'admin@my.fsu.edu', 'npeppa', 'ee1cca60e988b68e2ec02deaa6c052f309d9d14154eb7c5812b85e8f7bf4d36b81b0382f96cce5854506288615e2190e397ca2d56f0f4194b28b54be2893b5fc');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;


-- Dumping structure for table ajh11m_dis.area
DROP TABLE IF EXISTS `area`;
CREATE TABLE IF NOT EXISTS `area` (
  `area_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `area_name` varchar(50) NOT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table ajh11m_dis.area: ~12 rows (approximately)
DELETE FROM `area`;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` (`area_id`, `area_name`) VALUES
	(1, 'Instruction'),
	(2, 'Morphbank'),
	(3, 'Help Desk'),
	(4, 'Webdesign'),
	(5, 'MSRC'),
	(6, 'JELIS'),
	(7, 'Goldstein'),
	(8, 'Data Mgmt'),
	(9, 'Research'),
	(10, 'Front Desk'),
	(11, 'Institute'),
	(12, 'London Program');
/*!40000 ALTER TABLE `area` ENABLE KEYS */;


-- Dumping structure for table ajh11m_dis.assignment
DROP TABLE IF EXISTS `assignment`;
CREATE TABLE IF NOT EXISTS `assignment` (
  `ta_id` smallint(5) unsigned NOT NULL,
  `fac_id` smallint(5) unsigned NOT NULL,
  `crs_id` mediumint(8) unsigned NOT NULL,
  `asn_semester` enum('fall','spr','sum') NOT NULL,
  `asn_year` year(4) NOT NULL,
  `asn_reason` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`crs_id`,`fac_id`,`ta_id`),
  KEY `fk_assignment_faculty1_idx` (`fac_id`),
  KEY `fk_assignment_ta1_idx` (`ta_id`),
  KEY `fk_assignment_course1_idx` (`crs_id`),
  CONSTRAINT `fk_assignment_course1` FOREIGN KEY (`crs_id`) REFERENCES `course` (`crs_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_assignment_faculty1` FOREIGN KEY (`fac_id`) REFERENCES `faculty` (`fac_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_assignment_ta1` FOREIGN KEY (`ta_id`) REFERENCES `ta` (`ta_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ajh11m_dis.assignment: ~0 rows (approximately)
DELETE FROM `assignment`;
/*!40000 ALTER TABLE `assignment` DISABLE KEYS */;
INSERT INTO `assignment` (`ta_id`, `fac_id`, `crs_id`, `asn_semester`, `asn_year`, `asn_reason`) VALUES
	(1, 4, 11, 'fall', '2013', '');
/*!40000 ALTER TABLE `assignment` ENABLE KEYS */;


-- Dumping structure for table ajh11m_dis.course
DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `crs_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `crs_num` varchar(10) NOT NULL,
  `crs_name` varchar(100) NOT NULL,
  `crs_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`crs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ajh11m_dis.course: ~150 rows (approximately)
DELETE FROM `course`;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` (`crs_id`, `crs_num`, `crs_name`, `crs_description`) VALUES
	(1, 'CGS 2835', 'Interdisciplinary Web Development', NULL),
	(2, 'COP 2258', 'Problems Solving with Object-Oriented Programming', NULL),
	(3, 'IDC 2930r', 'Special Topics in Interdisciplinary Computing - Beginning Level (1 - 4)', NULL),
	(4, 'IDC 3931r', 'Special Topics in Interdisciplinary Computing - Intermediate Level (1 - 4)', NULL),
	(5, 'LIS 2780', 'Database Concepts', NULL),
	(6, 'LIS 3021', 'Information and Society', NULL),
	(7, 'LIS 3201', 'Research and Data Analysis for Information Professionals', NULL),
	(8, 'LIS 3267', 'Information Science', NULL),
	(9, 'LIS 3353', 'Technologies for Information Services', NULL),
	(10, 'LIS 3706', 'Information Systems and Services', NULL),
	(11, 'LIS 3781', 'Advanced Database Management', NULL),
	(12, 'LIS 3784', 'Information Organization and Communication', NULL),
	(13, 'LIS 3793', 'Information Architecture', NULL),
	(14, 'LIS 3946r', 'Field Study in Information Studies (1 - 6)', NULL),
	(15, 'LIS 4264', 'Systems Approach in the Information Environment', NULL),
	(16, 'LIS 4276', 'Quantitative Methods in Information Studies', NULL),
	(17, 'LIS 4277', 'Usability and Usefulness of Information Systems', NULL),
	(18, 'LIS 4301', 'Electronic Media Production', NULL),
	(19, 'LIS 4351', 'Interface Design', NULL),
	(20, 'LIS 4365', 'Advanced Web Applications', NULL),
	(21, 'LIS 4366', 'Web Site Development and Administration', NULL),
	(22, 'LIS 4368', 'Web Development with PHP', NULL),
	(23, 'LIS 4410', 'Societal Implications of the Information Age', NULL),
	(24, 'LIS 4480', 'Information Technology Leadership', NULL),
	(25, 'LIS 4481', 'Managing Information Resources and Services', NULL),
	(26, 'LIS 4482', 'Managing Networks and Telecommunications', NULL),
	(27, 'LIS 4488', 'Network Administration for the Information Professional', NULL),
	(28, 'LIS 4642', 'Electronic Information Sources and Services', NULL),
	(29, 'LIS 4701', 'Information Representation', NULL),
	(30, 'LIS 4708', 'Perspectives on Information Technology', NULL),
	(31, 'LIS 4770', 'Information and Image Management', NULL),
	(32, 'LIS 4774', 'Information Security', NULL),
	(33, 'LIS 4776', 'Advanced Health Informatics', NULL),
	(34, 'LIS 4785', 'Introduction to Health Informatics', NULL),
	(35, 'LIS 4905r', 'Directed Individual Study (1 - 3)', NULL),
	(36, 'LIS 4910', 'Information Technology Project', NULL),
	(37, 'LIS 4930r', 'Special Topics in Information Studies', NULL),
	(38, 'LIS 4938', 'Seminar in Information Studies', NULL),
	(39, 'LIS 4940r', 'Internship in Information Studies (1 - 6)', NULL),
	(40, 'LIS 4970r', 'Honors Work in Information Studies (1 - 6)', NULL),
	(41, 'LIS 5008', 'Advanced Online Searching', NULL),
	(42, 'LIS 5015', 'Teaching Interdisciplinary Computing (2 - 3)', NULL),
	(43, 'LIS 5020', 'Foundations of the Information Professions', NULL),
	(44, 'LIS 5105', 'Communities of Practice', NULL),
	(45, 'LIS 5112', 'History of Reading in Everyday Life', NULL),
	(46, 'LIS 5113', 'History of American Librarianship', NULL),
	(47, 'LIS 5203', 'Assessing Information Needs', NULL),
	(48, 'LIS 5241', 'International and Comparative Information Service', NULL),
	(49, 'LIS 5260', 'Information Science', NULL),
	(50, 'LIS 5263', 'Theory of Information Retrieval', NULL),
	(51, 'LIS 5270', 'Evaluating Networked Information Services and Systems', NULL),
	(52, 'LIS 5271', 'Research in Information Studies', NULL),
	(53, 'LIS 5273', 'Practical Library and Information Science Exploration', NULL),
	(54, 'LIS 5275', 'Usability Analysis', NULL),
	(55, 'LIS 5313', 'Digital Media: Concepts and Production', NULL),
	(56, 'LIS 5316', 'Information Graphics', NULL),
	(57, 'LIS 5362', 'Design and Production of Networked Multimedia', NULL),
	(58, 'LIS 5364', 'Web Site Development and Administration', NULL),
	(59, 'LIS 5367', 'Advanced Web Applications', NULL),
	(60, 'LIS 5403', 'Human Resource Management for Information Professionals', NULL),
	(61, 'LIS 5405', 'Leadership in Technology', NULL),
	(62, 'LIS 5408', 'Management of Information Organizations', NULL),
	(63, 'LIS 5411', 'Introduction to Information Policy', NULL),
	(64, 'LIS 5413', 'Seminar in Information Policy', NULL),
	(65, 'LIS 5416', 'Introduction to Legal Informatics', NULL),
	(66, 'LIS 5417', 'Introduction to Legal Resources', NULL),
	(67, 'LIS 5418', 'Introduction to Health Informatics', NULL),
	(68, 'LIS 5426', 'Planning, Evaluation and Financial Management', NULL),
	(69, 'LIS 5441', 'Leadership in Reading', NULL),
	(70, 'LIS 5442', 'Information Leadership', NULL),
	(71, 'LIS 5472', 'Digital Libraries', NULL),
	(72, 'LIS 5474', 'Business Information Needs and Sources', NULL),
	(73, 'LIS 5484', 'Introduction to Data Networks for Information Professionals', NULL),
	(74, 'LIS 5487', 'Information Systems Management', NULL),
	(75, 'LIS 5489', 'Network Administration', NULL),
	(76, 'LIS 5511', 'Management of Information Collections', NULL),
	(77, 'LIS 5512', 'School Collection Development and Management', NULL),
	(78, 'LIS 5513', 'Preservation of Information Materials', NULL),
	(79, 'LIS 5524', 'Instructional Role of the Informational Specialist', NULL),
	(80, 'LIS 5528', 'Storytelling for Information Professionals', NULL),
	(81, 'LIS 5564', 'Information Needs of Children', NULL),
	(82, 'LIS 5565', 'Information Needs of Young Adults', NULL),
	(83, 'LIS 5566', 'Multicultural Literature and Information Resources for Children and Young Adults', NULL),
	(84, 'LIS 5567', 'International Literature for Children and Young Adults', NULL),
	(85, 'LIS 5576', 'Information Needs of Adults', NULL),
	(86, 'LIS 5590', 'Museum Informatics', NULL),
	(87, 'LIS 5602', 'Marketing of Library and Information Services', NULL),
	(88, 'LIS 5603', 'Introduction to Information Services', NULL),
	(89, 'LIS 5631', 'Health Information Sources', NULL),
	(90, 'LIS 5661', 'Government Information', NULL),
	(91, 'LIS 5703', 'Information Organization', NULL),
	(92, 'LIS 5711', 'Cataloging and Classification', NULL),
	(93, 'LIS 5736', 'Indexing and Abstracting', NULL),
	(94, 'LIS 5737', 'Subject Analysis', NULL),
	(95, 'LIS 5771', 'Information and Image Management', NULL),
	(96, 'LIS 5782', 'Database Management Systems', NULL),
	(97, 'LIS 5786', 'Introduction to Information Architecture', NULL),
	(98, 'LIS 5787', 'Fundamentals of Metadata Theory and Practice', NULL),
	(99, 'LIS 5788', 'Management of Health Information Technology', NULL),
	(100, 'LIS 5916r', 'Issues in Information Studies (1 - 3)', NULL),
	(128, 'LIS5008', 'Advanced Online Searching', NULL),
	(129, 'LIS5020', 'Foundations Info Professions', NULL),
	(130, 'LIS5020', 'Foundations Info Professions', NULL),
	(131, 'LIS5105', 'Communities of Practice', NULL),
	(132, 'LIS5203', 'Assessing Information Needs', NULL),
	(133, 'LIS5203', 'Assessing Info Needs', NULL),
	(134, 'LIS5241', 'International & Comparative Information Service', NULL),
	(135, 'LIS5263', 'Theory of Information Retrieval', NULL),
	(136, 'LIS5270', 'Evaluation of Networked Information Services & Systems', NULL),
	(137, 'LIS5271', 'Research in Information Studies', NULL),
	(138, 'LIS5275', 'Usability Analysis', NULL),
	(139, 'LIS5313', 'Design & Production of Media Resources', NULL),
	(140, 'LIS5362', 'Design & Production of Network Multimedia', NULL),
	(141, 'LIS5364', 'Website Development & Administration', NULL),
	(142, 'LIS5367', 'Advanced Web Applications', NULL),
	(143, 'LIS5405', 'Leadership in Technology', NULL),
	(144, 'LIS5408', 'Management of Information Organizations', NULL),
	(145, 'LIS5411', 'Introduction to Information Policy', NULL),
	(146, 'LIS5417', 'Introduction to Legal Resources', NULL),
	(147, 'LIS5426', 'Planning, Evaluation & Financial Management', NULL),
	(148, 'LIS5441', 'Leadership in Reading', NULL),
	(149, 'LIS5442', 'Information Leadership', NULL),
	(150, 'LIS5442', 'Info Leadership', NULL),
	(151, 'LIS5472', 'Digital Libraries', NULL),
	(152, 'LIS5474', 'Business Information Needs & Sources', NULL),
	(153, 'LIS5484', 'Introduction to Data Networks for Information Professionals', NULL),
	(154, 'LIS5487', 'Information Systems Management', NULL),
	(155, 'LIS5489', 'Network Administration', NULL),
	(156, 'LIS5511', 'Management of Information Collections', NULL),
	(157, 'LIS5512', 'School Collection Development & Management', NULL),
	(158, 'LIS5524', 'Instructional Role of Information Specialist', NULL),
	(159, 'LIS5564', 'Information Needs of Children', NULL),
	(160, 'LIS5565', 'Information Needs of Young Adults', NULL),
	(161, 'LIS5566', 'Multicultural Literature for Youth', NULL),
	(162, 'LIS5567', 'International Literature for Children & Young Adults', NULL),
	(163, 'LIS5576', 'Information Needs of Adults', NULL),
	(164, 'LIS5590', 'Museum Informatics', NULL),
	(165, 'LIS5602', 'Marketing Library & Information Services', NULL),
	(166, 'LIS5603', 'Introduction to Information Services', NULL),
	(167, 'LIS5661', 'Government Information', NULL),
	(168, 'LIS5703', 'Information Organization', NULL),
	(169, 'LIS5711', 'Cataloging & Classification', NULL),
	(170, 'LIS5736', 'Indexing & Abstracting', NULL),
	(171, 'LIS5782', 'Database Management Systems', NULL),
	(172, 'LIS5786', 'Intro to Information Architecture', NULL),
	(173, 'LIS5787', 'Metadata for Information Professionals', NULL),
	(174, 'LIS5916', 'Health Informatics', NULL),
	(175, 'LIS5916', 'Health Info Sources', NULL),
	(176, 'LIS5916', 'Information Security', NULL),
	(177, 'LIS5916', 'Project Management for Information Professionals', NULL);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;


-- Dumping structure for table ajh11m_dis.course_skill
DROP TABLE IF EXISTS `course_skill`;
CREATE TABLE IF NOT EXISTS `course_skill` (
  `crs_id` mediumint(8) unsigned NOT NULL,
  `fac_id` smallint(5) unsigned NOT NULL,
  `skl_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`crs_id`,`fac_id`,`skl_id`),
  KEY `idx_course_skills_course` (`crs_id`),
  KEY `idx_course_skills_faculty` (`fac_id`),
  KEY `idx_course_skills_skill` (`skl_id`),
  CONSTRAINT `fk_course_skills_course` FOREIGN KEY (`crs_id`) REFERENCES `course` (`crs_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_course_skills_faculty` FOREIGN KEY (`fac_id`) REFERENCES `faculty` (`fac_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_course_skills_skill` FOREIGN KEY (`skl_id`) REFERENCES `skill` (`skl_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ajh11m_dis.course_skill: ~0 rows (approximately)
DELETE FROM `course_skill`;
/*!40000 ALTER TABLE `course_skill` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_skill` ENABLE KEYS */;


-- Dumping structure for table ajh11m_dis.faculty
DROP TABLE IF EXISTS `faculty`;
CREATE TABLE IF NOT EXISTS `faculty` (
  `fac_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `fac_fname` varchar(15) NOT NULL,
  `fac_lname` varchar(45) NOT NULL,
  `fac_email` varchar(100) NOT NULL,
  PRIMARY KEY (`fac_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ajh11m_dis.faculty: ~52 rows (approximately)
DELETE FROM `faculty`;
/*!40000 ALTER TABLE `faculty` DISABLE KEYS */;
INSERT INTO `faculty` (`fac_id`, `fac_fname`, `fac_lname`, `fac_email`) VALUES
	(1, 'Jonathan ', 'Adams', 'ladams@fsu.edu'),
	(2, 'Ken', 'Armstrong', ''),
	(3, 'Laura', 'Arpan', 'larpan@fsu.edu'),
	(4, 'Ken', 'Baldauf', ''),
	(5, 'Jane', 'Barrager', 'jane.barrager@cci.fsu.edu'),
	(6, 'Farhood', 'Basiri', ''),
	(7, 'Marion', 'Bogdanov', ''),
	(8, 'Gary', 'Burnett', 'gary.burnett@cci.fsu.edu'),
	(9, 'Kathleen', 'Burnett', 'kathy.burnett@cci.fsu.edu'),
	(10, 'Janet', 'Capps', ''),
	(11, 'Ted', 'Chaffin', ''),
	(12, 'Lenese', 'Colson', 'lmc07k@my.fsu.edu'),
	(13, 'Larry', 'Dennis', 'larry.dennis@cci.fsu.edu'),
	(14, 'Eliza', 'Dressang', 'edressang@fsu.edu'),
	(15, 'Nancy', 'Everhart', 'nancy.everhart@cci.fsu.edu'),
	(16, 'Harold', 'George', ''),
	(17, 'Leila', 'Gibradze', ''),
	(18, 'Melissa', 'Gross', 'melissa.gross@cci.fsu.edu'),
	(19, 'Gary ', 'Heald', 'gheald@fsu.edu'),
	(20, 'Joseph', 'Hemingway', ''),
	(21, 'Chris', 'Hinnant', 'chinnant@fsu.edu'),
	(22, 'Lynne', 'Hinnant', 'lynne.hinnant@cci.fsu.edu'),
	(23, 'Shuyuan', 'Ho', ''),
	(24, 'Corinne', 'Jorgensen', 'corinne.jorgensen@cci.fsu.edu'),
	(25, 'Peter', 'Jorgensen', 'peter.jorgensen@cci.fsu.edu'),
	(26, 'Mark', 'Jowett', 'mark.jowett@cci.fsu.edu'),
	(27, 'Michelle', 'Kazmer', 'michelle.kazmer@cci.fsu.edu'),
	(28, 'Kyunghye', 'Kim', ''),
	(29, 'Christie', 'Koontz', 'christie.koontz@cci.fsu.edu'),
	(30, 'Bowie', 'Kotrla', 'bowie.kotrla@cci.fsu.edu'),
	(31, 'Christopher', 'Landbeck', ''),
	(32, 'Don', 'Latham', 'don.latham@cci.fsu.edu'),
	(33, 'Diane', 'Leiva', ''),
	(34, 'Mia Liza', 'Lustria', 'mia.lustria@cci.fsu.edu'),
	(35, 'Marcia', 'Mardis', 'marcia.mardis@cci.fsu.edu'),
	(36, 'John', 'Marks', ''),
	(37, 'Paul', 'Marty', 'paul.marty@cci.fsu.edu'),
	(38, 'Casey', 'Mc Laughlin', ''),
	(39, 'Charles ', 'McClure', 'charles.mcclure@cci.fsu.edu'),
	(40, 'Geoffery', 'Miller', ''),
	(41, 'Stephen ', 'McDowell', 'smcdowel@fsu.edu'),
	(42, 'Lorri', 'Mon', 'lorri.mon@cci.fsu.edu'),
	(43, 'Howard', 'Mori', ''),
	(44, 'Sanghee', 'Oh', 'sanghee.oh@cci.fsu.edu'),
	(45, 'Mary', 'Prentice', ''),
	(46, 'Ebe', 'Randeree', 'ebe.randeree@cci.fsu.edu'),
	(47, 'Aimee', 'Reist', ''),
	(48, 'Greg', 'Riccardi', 'greg.riccardi@cci.fsu.edu'),
	(49, 'Besiki', 'Stvilia', 'besiki.stvilia@cci.fsu.edu'),
	(50, 'Linda', 'Swaine', 'linda.swaine@cci.fsu.edu'),
	(51, 'Richard', 'Urban', ''),
	(52, 'Casey', 'Yu', 'cy09g@my.fsu.edu');
/*!40000 ALTER TABLE `faculty` ENABLE KEYS */;


-- Dumping structure for table ajh11m_dis.skill
DROP TABLE IF EXISTS `skill`;
CREATE TABLE IF NOT EXISTS `skill` (
  `skl_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `skl_name` varchar(45) NOT NULL,
  `skl_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`skl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ajh11m_dis.skill: ~12 rows (approximately)
DELETE FROM `skill`;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` (`skl_id`, `skl_name`, `skl_description`) VALUES
	(1, 'grading', 'using/creating grading rubrics'),
	(2, 'programming', 'Any: PHP, C++, Java, ASP.NET, C#'),
	(3, 'interface design', ''),
	(4, 'wireframing', ''),
	(5, 'site mapping', ''),
	(6, 'UNIX/Linux', ''),
	(7, 'communication', 'oral and written English'),
	(8, 'database design', 'data modeling (ERDs), SQL'),
	(9, 'web design', 'HTML, CSS, and JavaScript (preferably)'),
	(10, 'computer networks', 'UNIX and/or Windows'),
	(11, 'English', 'proficiency in English'),
	(12, 'children\'s literature', '');
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;


-- Dumping structure for table ajh11m_dis.ta
DROP TABLE IF EXISTS `ta`;
CREATE TABLE IF NOT EXISTS `ta` (
  `ta_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `ta_fname` varchar(20) NOT NULL,
  `ta_lname` varchar(30) NOT NULL,
  `ta_email` varchar(100) NOT NULL,
  `ta_teaching_interests` text,
  `ta_research_interests` text,
  PRIMARY KEY (`ta_id`),
  UNIQUE KEY `ta_email` (`ta_email`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ajh11m_dis.ta: ~26 rows (approximately)
DELETE FROM `ta`;
/*!40000 ALTER TABLE `ta` DISABLE KEYS */;
INSERT INTO `ta` (`ta_id`, `ta_fname`, `ta_lname`, `ta_email`, `ta_teaching_interests`, `ta_research_interests`) VALUES
	(1, 'Jung Hoon', 'Baeg', 'jhb6536@my.fsu.edu', NULL, NULL),
	(2, 'Laura', 'Brenkus', 'lib03@my.fsu.edu', NULL, NULL),
	(3, 'Lisandra ', 'Carmichael', 'lc09h@my.fsu.edu', NULL, NULL),
	(4, 'Wonchan', 'Choi', 'wc10d@my.fsu.edu', NULL, NULL),
	(5, 'Laura Edythe', 'Coleman', 'lsc10@my.fsu.edu', NULL, NULL),
	(6, 'Lenese', 'Colson', 'lmc07k@my.fsu.edu', NULL, NULL),
	(7, 'Teralee', 'El Basri', 'tee07@my.fsu.edu', NULL, NULL),
	(8, 'Aaron', 'Elkins', 'aje11b@my.fsu.edu', NULL, NULL),
	(9, 'Amelia', 'Gibson', 'and04g@my.fsu.edu', NULL, NULL),
	(10, 'Micah', 'Gomez', 'mag04s@my.fsu.edu', NULL, NULL),
	(11, 'Jon', 'Hollister', 'jmh09k@my.fsu.edu', NULL, NULL),
	(12, 'Aisha', 'Johnson', 'amj04j@my.fsu.edu', NULL, NULL),
	(13, 'Ji Hei', 'Kang', 'jk11e@my.fsu.edu', NULL, NULL),
	(14, 'Joung Hwa (Joy)', 'Koo', 'noemail@my.fsu.edu', NULL, NULL),
	(15, 'Chris', 'Landbeck', 'crl05d@my.fsu.edu', NULL, NULL),
	(16, 'DongJoon', 'Lee', 'dl10e@my.fsu.edu', NULL, NULL),
	(17, 'Jongwook', 'Lee', 'nadoopro@gmail.com', NULL, NULL),
	(18, 'Jinxuan (Jenny)', 'Ma', 'jm07f@my.fsu.edu', NULL, NULL),
	(19, 'Min Soon', 'Park', 'mp11j@my.fsu.edu', NULL, NULL),
	(20, 'Abigail', 'Phillips', 'alp07@my.fsu.edu', NULL, NULL),
	(21, 'Nathanial', 'Ramos', 'nr12c@my.fsu.edu', NULL, NULL),
	(22, 'Rienne', 'Saludo', 'apw06@my.fsu.edu', NULL, NULL),
	(23, 'Julia ', 'Skinner', 'js11j@my.fsu.edu', NULL, NULL),
	(24, 'Adam', 'Worrall', 'rgs10@my.fsu.edu', NULL, NULL),
	(25, 'Shuheng ', 'Wu', 'sw09f@my.fsu.edu', NULL, NULL),
	(26, 'Casey ', 'Yu', 'cy09g@my.fsu.edu', NULL, NULL);
/*!40000 ALTER TABLE `ta` ENABLE KEYS */;


-- Dumping structure for table ajh11m_dis.ta_area
DROP TABLE IF EXISTS `ta_area`;
CREATE TABLE IF NOT EXISTS `ta_area` (
  `area_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ta_id` smallint(5) unsigned NOT NULL,
  `area_fte` decimal(3,2) NOT NULL,
  `area_semester` enum('fall','spr','sum') NOT NULL,
  `area_year` year(4) NOT NULL,
  PRIMARY KEY (`area_id`,`ta_id`),
  KEY `idx_area_ta` (`ta_id`),
  CONSTRAINT `fk_area_ta` FOREIGN KEY (`ta_id`) REFERENCES `ta` (`ta_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_area_ta_area` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ajh11m_dis.ta_area: ~0 rows (approximately)
DELETE FROM `ta_area`;
/*!40000 ALTER TABLE `ta_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `ta_area` ENABLE KEYS */;


-- Dumping structure for table ajh11m_dis.ta_skill
DROP TABLE IF EXISTS `ta_skill`;
CREATE TABLE IF NOT EXISTS `ta_skill` (
  `ta_id` smallint(5) unsigned NOT NULL,
  `skl_id` smallint(5) unsigned NOT NULL,
  `tskl_rating` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`ta_id`,`skl_id`),
  KEY `idx_ta_skills_ta` (`ta_id`),
  KEY `idx_ta_skills_skill` (`skl_id`),
  CONSTRAINT `fk_ta_skills_skill` FOREIGN KEY (`skl_id`) REFERENCES `skill` (`skl_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ta_skills_ta` FOREIGN KEY (`ta_id`) REFERENCES `ta` (`ta_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ajh11m_dis.ta_skill: ~0 rows (approximately)
DELETE FROM `ta_skill`;
/*!40000 ALTER TABLE `ta_skill` DISABLE KEYS */;
/*!40000 ALTER TABLE `ta_skill` ENABLE KEYS */;


-- Dumping structure for trigger ajh11m_dis.TASkillCheck_Ins
DROP TRIGGER IF EXISTS `TASkillCheck_Ins`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `TASkillCheck_Ins` BEFORE INSERT ON `ta_skill` FOR EACH ROW BEGIN
	SET NEW.tskl_rating =GREATEST(LEAST(NEW.tskl_rating,5),0);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger ajh11m_dis.TASkillCheck_Update
DROP TRIGGER IF EXISTS `TASkillCheck_Update`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `TASkillCheck_Update` BEFORE UPDATE ON `ta_skill` FOR EACH ROW BEGIN
	/*SET NEW.tskl_rating =GREATEST(LEAST(OLD.tskl_rating,5),0);*/
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
